<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>nwu.ac.za</groupId>
		<artifactId>contact</artifactId>
		<version>3.8.0</version>
	</parent>

	<artifactId>contact-details</artifactId>
	<packaging>war</packaging>
	<name>contact-details</name>

	<prerequisites>
		<maven>3</maven>
	</prerequisites>

	<properties>
		<vaadin.version>7.7.6</vaadin.version>
		<vaadin.plugin.version>7.7.6</vaadin.plugin.version>
		<jetty.plugin.version>9.3.9.v20160517</jetty.plugin.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<maven.compiler.source>1.8</maven.compiler.source>
		<maven.compiler.target>1.8</maven.compiler.target>
		<!-- If there are no local customisations, this can also be "fetch" or 
			"cdn" -->
		<vaadin.widgetset.mode>local</vaadin.widgetset.mode>
	</properties>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>com.vaadin</groupId>
				<artifactId>vaadin-bom</artifactId>
				<version>${vaadin.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>
			<dependency>
				<groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
            </dependency>	
		<dependency>
			<groupId>nwu.comp-service</groupId>
			<artifactId>notification-api</artifactId>
		</dependency>
		<dependency>
			<groupId>nwu.enabling.capabilities.supporting.service</groupId>
			<artifactId>supporting-services-api</artifactId>
		</dependency>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>3.0.1</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.vaadin</groupId>
			<artifactId>vaadin-server</artifactId>
		</dependency>
		<dependency>
			<groupId>com.vaadin</groupId>
			<artifactId>vaadin-push</artifactId>
		</dependency>
		<dependency>
			<groupId>com.vaadin</groupId>
			<artifactId>vaadin-client-compiled</artifactId>
		</dependency>
		<dependency>
			<groupId>com.vaadin</groupId>
			<artifactId>vaadin-themes</artifactId>
		</dependency>
		<dependency>
			<groupId>za.ac.nwu.pontoon</groupId>
			<artifactId>identity-vss-api</artifactId>
		</dependency>
		<dependency>
			<groupId>za.ac.nwu.pontoon</groupId>
			<artifactId>student-vss-api</artifactId>
		</dependency>
		<dependency>
			<groupId>nwu.ac.za</groupId>
			<artifactId>hr-employeeinformation-service</artifactId>
		</dependency>
		<dependency>
			<groupId>nwu.ac.za</groupId>
			<artifactId>nwu-vaadin-framework</artifactId>
			<exclusions>
				<exclusion>
					<groupId>za.ac.nwu.pontoon</groupId>
					<artifactId>framework-api-utils</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>nwu.ac.za</groupId>
			<artifactId>country-information-service</artifactId>
		</dependency>
		<dependency>
			<groupId>commons-lang</groupId>
			<artifactId>commons-lang</artifactId>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
			<version>${jackson-databind.version}</version>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-dependency-plugin</artifactId>
				<version>3.0.1</version>
				<executions>
					<execution>
						<id>unpack</id>
						<phase>package</phase>
						<goals>
							<goal>unpack</goal>
						</goals>
						<configuration>
							<artifactItems>
								<artifactItem>
									<groupId>nwu.ac.za</groupId>
									<artifactId>nwu-vaadin-framework</artifactId>
									<overWrite>true</overWrite>
									<includes>**/*tests-valo-facebook/*.css,**/*tests-valo-facebook/*.scss</includes>
									<outputDirectory>${project.basedir}/src/main/webapp/VAADIN</outputDirectory>
									<destFileName>nwu-theme.jar</destFileName>
								</artifactItem>
							</artifactItems>
							<outputDirectory>${project.build.directory}/wars</outputDirectory>
							<overWriteReleases>false</overWriteReleases>
							<overWriteSnapshots>true</overWriteSnapshots>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-clean-plugin</artifactId>
				<version>3.0.0</version>
				<!-- Clean up also any pre-compiled themes -->
				<configuration>
					<filesets>
						<fileset>
							<directory>src/main/webapp/VAADIN/themes</directory>
							<includes>
								<include>**/styles.css</include>
								<include>**/styles.scss.cache</include>
							</includes>
						</fileset>
					</filesets>
				</configuration>
			</plugin>

			<!-- The Jetty plugin allows us to easily test the development build by 
				running jetty:run on the command line. -->
			<plugin>
				<groupId>org.eclipse.jetty</groupId>
				<artifactId>jetty-maven-plugin</artifactId>
				<version>${jetty.plugin.version}</version>
				<configuration>
					<scanIntervalSeconds>2</scanIntervalSeconds>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<configuration>
					<failOnMissingWebXml>false</failOnMissingWebXml>
					<!-- Exclude an unnecessary file generated by the GWT compiler. -->
					<packagingExcludes>WEB-INF/classes/VAADIN/widgetsets/WEB-INF/**</packagingExcludes>
					<webResources>
						<resource>
							<filtering>true</filtering>
							<targetPath>WEB-INF</targetPath>
							<directory>src/main/resources</directory>
							<includes>
								<include>config.properties</include>
							</includes>
						</resource>
					</webResources>
				</configuration>
			</plugin>
			<!-- JBoss AS plugin to deploy war - Jenkins uses it for undeployment -->
			<plugin>
				<groupId>org.jboss.as.plugins</groupId>
				<artifactId>jboss-as-maven-plugin</artifactId>
				<configuration>
					<!-- Regex expression allowing undeploy of different versions via Jenkins -->
					<match-pattern>contact-details.*\.war</match-pattern>
				</configuration>
			</plugin>

		</plugins>

		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
			</resource>
		</resources>
	</build>

	<profiles>
		<profile>
			<!-- Vaadin pre-release repositories -->
			<id>vaadin-prerelease</id>
			<activation>
				<activeByDefault>false</activeByDefault>
			</activation>

			<repositories>
				<repository>
					<id>vaadin-prereleases</id>
					<url>http://maven.vaadin.com/vaadin-prereleases</url>
				</repository>
				<repository>
					<id>vaadin-snapshots</id>
					<url>https://oss.sonatype.org/content/repositories/vaadin-snapshots/</url>
					<releases>
						<enabled>false</enabled>
					</releases>
					<snapshots>
						<enabled>true</enabled>
					</snapshots>
				</repository>
			</repositories>
			<pluginRepositories>
				<pluginRepository>
					<id>vaadin-prereleases</id>
					<url>http://maven.vaadin.com/vaadin-prereleases</url>
				</pluginRepository>
				<pluginRepository>
					<id>vaadin-snapshots</id>
					<url>https://oss.sonatype.org/content/repositories/vaadin-snapshots/</url>
					<releases>
						<enabled>false</enabled>
					</releases>
					<snapshots>
						<enabled>true</enabled>
					</snapshots>
				</pluginRepository>
			</pluginRepositories>
		</profile>
		<profile>
			<id>testnocas</id>
			<properties>
				<ws_configmanagement_read_username>configservicetst</ws_configmanagement_read_username>
				<ws_configmanagement_read_username_password>wIeJ3zwfNC0OiU6KJ4AQCA==</ws_configmanagement_read_username_password>
				<ws_configmanagement_read_database>V_TEST</ws_configmanagement_read_database>
				<!-- -->
				<environment>https://workflow7tst.nwu.ac.za/</environment>
				<HRServiceUser>authtest</HRServiceUser>
				<HRServicePassword>asfa9HsBOxRK2Mle7D0QoA==</HRServicePassword>
				<AllowUnivNumberInput>false</AllowUnivNumberInput>
				<!-- IAPI-APP Webservice properties, based on environment -->
				<ws_iapiapp_read_username>iapiappreadtest</ws_iapiapp_read_username>
				<ws_iapiapp_read_username_password>8KlMC6R0M2jC95mYzG4nvQ==</ws_iapiapp_read_username_password>
				<ws_iapiapp_crud_username>iapiappwritetest</ws_iapiapp_crud_username>
				<ws_iapiapp_crud_username_password>dlE1U0aH2SGAWWKWq8rhhA==</ws_iapiapp_crud_username_password>
				<!-- SAPI-APP Webservice properties, based on environment -->
				<ws_sapiapp_read_username>sapiappreadtest</ws_sapiapp_read_username>
				<ws_sapiapp_read_username_password>sp@ssw0rd</ws_sapiapp_read_username_password>

				<runtimeEnvironment>test</runtimeEnvironment>

				<sms.provider.password>9635f9bd</sms.provider.password>
				<sms.provider.username>nwufinance</sms.provider.username>

				<notificationcomp_crud_username>notificationcrudtst</notificationcomp_crud_username>
				<notificationcomp_crud_password>G7jpNBBwTRJuFZ5BpcZxY4rI/fnVWEfgsDDWI8NvvPY=</notificationcomp_crud_password>
			</properties>
		</profile>
		<profile>
			<id>test</id>
			<properties>
				<environment>https://workflow7tst.nwu.ac.za/</environment>
				<HRServiceUser>authtest</HRServiceUser>
				<HRServicePassword>asfa9HsBOxRK2Mle7D0QoA==</HRServicePassword>
				<AllowUnivNumberInput>false</AllowUnivNumberInput>
				<!-- IAPI-APP Webservice properties, based on environment -->
				<ws_iapiapp_read_username>iapiappreadtest</ws_iapiapp_read_username>
				<ws_iapiapp_read_username_password>8KlMC6R0M2jC95mYzG4nvQ==</ws_iapiapp_read_username_password>
				<ws_iapiapp_crud_username>iapiappwritetest</ws_iapiapp_crud_username>
				<ws_iapiapp_crud_username_password>dlE1U0aH2SGAWWKWq8rhhA==</ws_iapiapp_crud_username_password>
				<!-- SAPI-APP Webservice properties, based on environment -->
				<ws_sapiapp_read_username>sapiappreadtest</ws_sapiapp_read_username>
				<ws_sapiapp_read_username_password>sp@ssw0rd</ws_sapiapp_read_username_password>
				<!-- ConfigurationService User name and password for QA Env -->
				<ws_configmanagement_read_username>configservicetst</ws_configmanagement_read_username>
				<ws_configmanagement_read_username_password>wIeJ3zwfNC0OiU6KJ4AQCA==</ws_configmanagement_read_username_password>
				<ws_configmanagement_read_database>V_TEST</ws_configmanagement_read_database>

				<runtimeEnvironment>test</runtimeEnvironment>
				<sms.provider.password>9635f9bd</sms.provider.password>

				<sms.provider.password>9635f9bd</sms.provider.password>
				<sms.provider.username>nwufinance</sms.provider.username>

				<notificationcomp_crud_username>notificationcrudtst</notificationcomp_crud_username>
				<notificationcomp_crud_password>G7jpNBBwTRJuFZ5BpcZxY4rI/fnVWEfgsDDWI8NvvPY=</notificationcomp_crud_password>
			</properties>
			<!-- TEC-736 : CAS Implementation -->
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-war-plugin</artifactId>
						<configuration>
							<webXml>src/main/resources/assembly/test/web.xml</webXml>
						</configuration>
					</plugin>
				</plugins>
			</build>
			<dependencies>
				<!-- TEC-736 : CAS Implementation -->
				<dependency>
					<groupId>org.jasig.cas.client</groupId>
					<artifactId>cas-client-core</artifactId>
				</dependency>
				<dependency>
					<groupId>org.jasig.cas.client</groupId>
					<artifactId>cas-client-integration-jboss</artifactId>
				</dependency>
			</dependencies>
		</profile>
		<profile>
			<id>qa-env</id>
			<!-- <activation> -->
			<!-- <activeByDefault>true</activeByDefault> -->
			<!-- </activation> -->
			<properties>
				<environment>https://wfd-dc1-rh7.nwu.ac.za</environment>
				<HRServiceUser>authtest</HRServiceUser>
				<HRServicePassword>asfa9HsBOxRK2Mle7D0QoA==</HRServicePassword>
				<AllowUnivNumberInput>true</AllowUnivNumberInput>
				<!-- IAPI-APP Webservice properties, based on environment -->
				<ws_iapiapp_read_username>iapiappreadqa</ws_iapiapp_read_username>
				<ws_iapiapp_read_username_password>8KlMC6R0M2jC95mYzG4nvQ==</ws_iapiapp_read_username_password>
				<ws_iapiapp_crud_username>iapiappwriteqa</ws_iapiapp_crud_username>
				<ws_iapiapp_crud_username_password>dlE1U0aH2SGAWWKWq8rhhA==</ws_iapiapp_crud_username_password>
				<!-- SAPI-APP Webservice properties, based on environment -->
				<ws_sapiapp_read_username>sapiappreadqa</ws_sapiapp_read_username>
				<ws_sapiapp_read_username_password>p@ssw0rd</ws_sapiapp_read_username_password>
				<runtimeEnvironment>qa</runtimeEnvironment>

				<ws_configmanagement_read_username>configserviceqa</ws_configmanagement_read_username>
				<ws_configmanagement_read_username_password>8KlMC6R0M2jC95mYzG4nvQ==</ws_configmanagement_read_username_password>
				<ws_configmanagement_read_database>V_TEST</ws_configmanagement_read_database>

				<sms.provider.password>9635f9bd</sms.provider.password>
				<sms.provider.username>nwufinance</sms.provider.username>

				<notificationcomp_crud_username>notificationcrudqa</notificationcomp_crud_username>
				<notificationcomp_crud_password>dlE1U0aH2SGAWWKWq8rhhA==</notificationcomp_crud_password>
			</properties>
		</profile>
		<profile>
			<id>prod</id>
			<properties>
				<environment>https://workflow7prd.nwu.ac.za</environment>
				<HRServiceUser>authprod</HRServiceUser>
				<HRServicePassword>authPr0d@321</HRServicePassword>
				<AllowUnivNumberInput>false</AllowUnivNumberInput>
				<!-- IAPI-APP Webservice properties, based on environment -->
				<ws_iapiapp_read_username>iapiappreadprod</ws_iapiapp_read_username>
				<ws_iapiapp_read_username_password>Bigyz1P0+IhiuiN8SncgjA==</ws_iapiapp_read_username_password>
				<ws_iapiapp_crud_username>iapiappwriteprod</ws_iapiapp_crud_username>
				<ws_iapiapp_crud_username_password>MBWgXaGUtgDXpmdGjhTaUsdYgHzYvbpCnFY4hPB/Q4A=</ws_iapiapp_crud_username_password>
				<!-- SAPI-APP Webservice properties, based on environment -->
				<ws_sapiapp_read_username>sapiappreadprod</ws_sapiapp_read_username>
				<ws_sapiapp_read_username_password>5p@ssw0rd4pr0dr</ws_sapiapp_read_username_password>

				<ws_configmanagement_read_username>configserviceprd</ws_configmanagement_read_username>
				<ws_configmanagement_read_username_password>gJM3KsP+G5pdHIjqV27Kuvmrj2jNEEy2wR49r60ioDI=</ws_configmanagement_read_username_password>
				<ws_configmanagement_read_database>V_PROD</ws_configmanagement_read_database>

				<sms.provider.password>9635f9bd</sms.provider.password>
				<sms.provider.username>nwufinance</sms.provider.username>


				<notificationcomp_crud_username>notificationcrudprd</notificationcomp_crud_username>
				<notificationcomp_crud_password>RiuMXd0vPtZY4H9bCPzaipZEjAuLk5pELyDUk/mPuQk=</notificationcomp_crud_password>

				<runtimeEnvironment>prod</runtimeEnvironment>
			</properties>
			<!-- TEC-736 : CAS Implementation -->
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-war-plugin</artifactId>
						<configuration>
							<webXml>src/main/resources/assembly/prod/web.xml</webXml>
						</configuration>
					</plugin>
				</plugins>
			</build>
			<dependencies>
				<!-- TEC-736 : CAS Implementation -->
				<dependency>
					<groupId>org.jasig.cas.client</groupId>
					<artifactId>cas-client-core</artifactId>
				</dependency>
				<dependency>
					<groupId>org.jasig.cas.client</groupId>
					<artifactId>cas-client-integration-jboss</artifactId>
				</dependency>
			</dependencies>
		</profile>
	</profiles>

</project>
