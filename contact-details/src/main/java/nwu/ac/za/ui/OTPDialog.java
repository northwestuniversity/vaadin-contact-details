package nwu.ac.za.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import ac.za.nwu.core.contact.dto.EntityPhoneNumberInfo;
import ac.za.nwu.core.contact.dto.EntityVirtualAddressInfo;
import ac.za.nwu.notification.dto.body.MessageBodyInfo;
import ac.za.nwu.notification.dto.body.TemplateInfo;
import ac.za.nwu.notification.dto.body.wrappers.ParameterNameWrapperInfo;
import ac.za.nwu.notification.dto.body.wrappers.ParameterValueWrapperInfo;
import ac.za.nwu.notification.dto.body.wrappers.SubjectHeaderInfo;
import ac.za.nwu.notification.dto.notifications.NotificationInfo;
import ac.za.nwu.notification.dto.notifications.email.EmailInfo;
import ac.za.nwu.notification.dto.notifications.email.wrappers.LanguageAndTemplateParamWrapperInfo;
import ac.za.nwu.notification.dto.notifications.email.wrappers.RecipientWrapperInfo;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.OperationFailedException;
import nwu.ac.za.config.ApplicationConfig;
import nwu.ac.za.service.ConfigurationProxyService;
import nwu.ac.za.service.NotificationCompProxyService;
import nwu.ac.za.service.OTPProxyService;
import nwu.ac.za.service.SMSProxyService;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.ac.za.ui.vaadin.infc.ContactDetailStudentInfc;
import nwu.ac.za.util.ContactDetailMessages;
import nwu.enabling.capabilities.supporting.service.otp.dto.OTPInfo;

public class OTPDialog extends Window {
    private final Logger log = LoggerFactory.getLogger(OTPDialog.class.getName());

    private String sendSMS = "SEND.SMS";
    private String smsCellnumber = "SEND.TEST.SMS.CELLNUMBER";
    private static final String SUCCESS = "success";
    private static final String FAILIURE = "failiure";

    private ApplicationConfig applicationConfig;

    private VerticalLayout verticalLayout = new VerticalLayout();
    private HorizontalLayout otpNumberLayout = new HorizontalLayout();
    private Label otpNumberLabel = new Label();
    private TextField otpNumberTextField = new TextField();

    private HorizontalLayout otpEmailLayout = new HorizontalLayout();
    private Label otpEmailLabel = new Label();
    private TextField otpEmailTextField = new TextField();

    private HorizontalLayout otpActionButtonLayout = new HorizontalLayout();
    private Button submit = new Button();
    private Button cancel = new Button();

    private VerticalLayout otpResendLayout = new VerticalLayout();
    private Label otpDisclaimer = new Label();
    private Label otpResendLabel = new Label();
    private Button otpResend = new Button();

    private OTPInfo otpSMSInfo = new OTPInfo();
    private OTPInfo otpEmailInfo = new OTPInfo();
    Object[] SMSparams = new Object[5];

    private HashMap<String, String> allSMSConfig = new HashMap<>();
    private ConfigurationProxyService configurationServiceProxyService;
    private SMSProxyService smsProxyService;
    private ContactSPCrudUI contactSPCrudUI;
    
    private NotificationCompProxyService notificationService;
    private String lookupUserCorLang;
    private String emailAddress;

    public OTPDialog(EntityPhoneNumberInfo entityPhoneNumberInfo, EntityVirtualAddressInfo virtualAddressInfo, String appPropertyFileName, String lookupUser, String authenticatedUser, String isCellNumberChanged, String isEmailChanged, ContactSPCrudUI crudUI, ContactDetailStudentInfc contentViewDetail, String corLang, String emailAddToBeUsed) {
    	lookupUserCorLang = corLang;
    	emailAddress = emailAddToBeUsed;
    	this.contactSPCrudUI = crudUI;
        applicationConfig = ApplicationConfig.getInstance(appPropertyFileName);
        OTPProxyService otpProxyService = new OTPProxyService(applicationConfig, appPropertyFileName);

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setCurrentDate(new Date());

        if (isCellNumberChanged != null) {
            otpNumberLabel.setCaption("Enter OTP sent to" + " " + entityPhoneNumberInfo.getPhoneNumber());
        }

        if (isEmailChanged != null) {
            otpEmailLabel.setCaption("Enter OTP sent to" + " " + virtualAddressInfo.getVirtualAddress());
        }
        otpDisclaimer.setCaption("OTP expires in 3 hours");
        otpResendLabel.setCaption("Please click resend OTP when you are ready");

        otpResendLayout.addComponents(otpResendLabel, otpResend);
        otpResendLayout.setMargin(false);
        otpResendLayout.setSpacing(false);

        otpResend.setCaption(ContactDetailMessages.getString("otp.resend.button"));
        submit.setCaption("Submit");
        submit.setStyleName(ValoTheme.BUTTON_PRIMARY);
        cancel.setCaption("Cancel");

        otpNumberLayout.addComponents(otpNumberLabel, otpNumberTextField);
        otpNumberLayout.setSpacing(true);
        otpNumberLayout.setComponentAlignment(otpNumberLabel, Alignment.MIDDLE_CENTER);
        otpNumberLayout.setComponentAlignment(otpNumberTextField, Alignment.MIDDLE_CENTER);

        otpEmailLayout.addComponents(otpEmailLabel, otpEmailTextField);
        otpEmailLayout.setComponentAlignment(otpEmailLabel, Alignment.MIDDLE_CENTER);
        otpEmailLayout.setComponentAlignment(otpEmailTextField, Alignment.MIDDLE_CENTER);
        otpEmailLayout.setSpacing(true);

        otpActionButtonLayout.addComponents(cancel, submit);
        otpActionButtonLayout.setSpacing(true);

        verticalLayout.addComponents(otpNumberLayout, otpEmailLayout, otpDisclaimer, otpResendLayout, otpActionButtonLayout);
        verticalLayout.setSpacing(true);
        verticalLayout.setMargin(true);

        if (isCellNumberChanged == null) {
            otpNumberLayout.setVisible(false);
            otpNumberTextField.setRequired(false);
        } else if (isEmailChanged == null) {
            otpEmailLayout.setVisible(false);
            otpEmailTextField.setVisible(false);
        }

        setContent(verticalLayout);
        
        notificationService = new NotificationCompProxyService(applicationConfig, applicationConfig.getAppPropertyFileName());
        configurationServiceProxyService = new ConfigurationProxyService(applicationConfig, applicationConfig.getAppPropertyFileName());
        smsProxyService = new SMSProxyService(applicationConfig, applicationConfig.getAppPropertyFileName());
        allSMSConfig = configurationServiceProxyService.getAllSMSConfig(authenticatedUser);

        if (isCellNumberChanged != null) {
            otpSMSInfo = otpProxyService.generateTOTPForClientKey(lookupUser+"~SMS", contextInfo);
            SMSparams = new Object[] {otpSMSInfo.getPassword()};
        }
        if (isEmailChanged != null) {
            otpEmailInfo = otpProxyService.generateTOTPForClientKey(lookupUser+"~EMAIL", contextInfo);
        }
        if (allSMSConfig.get(sendSMS).equalsIgnoreCase("true")) {
            String SMSmessage = "Your verification code is:" + " " + SMSparams[0];

            String dailcode = contactSPCrudUI.getShortDialCode(contentViewDetail.getCellphoneCountryCode());
            String phoneNumber = contentViewDetail.getCellphoneNumber().getValue();
            if (isCellNumberChanged != null) {
                smsProxyService.getSMSNotification(applicationConfig.getSMS_PROVIDER_USERNAME(), applicationConfig.getSMS_PROVIDER_PASSWORD(), dailcode, phoneNumber, SMSmessage);
            }
            if(isEmailChanged !=null) {
                sendEmail();
            }
        }

        this.submit.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                String userOTPNum = otpNumberTextField.getValue();
                String userOTPEmail = otpEmailTextField.getValue();

                if (userOTPNum.trim().length() == 0 && isCellNumberChanged != null) {
                    throw new VaadinUIException("Please enter the OTP sent via SMS", VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
                }

                if (userOTPEmail.trim().length() == 0 && isEmailChanged != null) {
                    throw new VaadinUIException("Please enter the OTP sent via email", VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
                }

                if (!userOTPNum.equals(otpSMSInfo.getPassword()) && isCellNumberChanged != null) {
                    throw new VaadinUIException("Incorrect SMS OTP submitted, please try again", VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
                }

                if (!userOTPEmail.equals(otpEmailInfo.getPassword()) && isEmailChanged != null) {
                    throw new VaadinUIException("Incorrect Email OTP submitted, please try again", VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
                }

                if (userOTPNum.equals(otpSMSInfo.getPassword()) || userOTPEmail.equals(otpEmailInfo.getPassword())) {

                    // Why is this needed
//                    if (userOTPNum.equals(otpSMSInfo.getPassword())) {
//                        OTPInfo otpInfoSMS = new OTPInfo(userOTPNum, otpSMSInfo.getHmac());
//                        if (isCellNumberChanged != null) {
//                            if (otpProxyService.validateTOTPForClientKey(lookupUser + "~SMS", otpInfoSMS, contextInfo)) {
//
//                            } else {
//                                throw new VaadinUIException("Unable to validate SMS OTP", VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.SERVERSIDE);
//                            }
//                        }
//                    }
//
//                    if (userOTPEmail.equals(otpEmailInfo.getPassword())) {
//                        OTPInfo otpInfoEmail = new OTPInfo(userOTPEmail, otpEmailInfo.getHmac());
//                        if (isEmailChanged != null) {
//                            if (otpProxyService.validateTOTPForClientKey(lookupUser + "~EMAIL", otpInfoEmail, contextInfo)) {
//                            } else {
//                                throw new VaadinUIException("Unable to validate Email OTP", VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.SERVERSIDE);
//                            }
//                        }
//                    }

                    contactSPCrudUI.continueSave();
                }
            }
        });

        this.otpResend.addClickListener(new Button.ClickListener() {


			@Override
            public void buttonClick(Button.ClickEvent event) {

                if (allSMSConfig.get(sendSMS).equalsIgnoreCase("true")) {
                    String SMSmessage = "Your verification code is:" + " " + SMSparams[0];

                    String dailcode = contactSPCrudUI.getShortDialCode(contentViewDetail.getCellphoneCountryCode());
                    String phoneNumber = contentViewDetail.getCellphoneNumber().getValue();
                    if (isCellNumberChanged != null) {
                        smsProxyService.getSMSNotification(applicationConfig.getSMS_PROVIDER_USERNAME(),
                                applicationConfig.getSMS_PROVIDER_PASSWORD(), dailcode, phoneNumber, SMSmessage);
                    }
                    if(isEmailChanged !=null) {


                        sendEmail();
                    }
                }
            }
        });


        this.cancel.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Collection<Window> windows = UI.getCurrent().getWindows();
                if(windows != null){
                    for(Window window : windows){
                        UI.getCurrent().removeWindow(window);
                    }
                }
            }
        });
    }

    public String sendEmail() {
        HashMap<String, String> replacementValues = new HashMap<>();
        replacementValues.put("OTP", otpEmailInfo.getPassword());
        
    
    	NotificationInfo notificationInfo = new NotificationInfo();
    	EmailInfo emailInfo = new EmailInfo();
    	emailInfo.setFromAddress("DIY-do-not-reply@nwu.ac.za");
    	
    	MessageBodyInfo messageBodyInfo = new MessageBodyInfo();
    	
    	TemplateInfo templateInfo = new TemplateInfo();
    	templateInfo.setApplication("CONTACT-DETAILS");
    	templateInfo.setMessageCatalogKey("cd.otp.email.body");
    	templateInfo.setVersion("GLOBAL");
		messageBodyInfo.setTemplateInfo(templateInfo );
		
		emailInfo.setMessageBodyInfo(messageBodyInfo );
		
    	List<String> replyToAddresses = new ArrayList<String>();
    	replyToAddresses.add("DIY-do-not-reply@nwu.ac.za");
		emailInfo.setReplyToAddresses(replyToAddresses );
    	
		SubjectHeaderInfo subjectHeaderInfo = new SubjectHeaderInfo();
		TemplateInfo headerTemplate = new TemplateInfo();
		headerTemplate.setApplication("CONTACT-DETAILS");
		headerTemplate.setMessageCatalogKey("cd.otp.email.subject");
		headerTemplate.setVersion("GLOBAL");
		subjectHeaderInfo.setTemplateInfo(headerTemplate );
		emailInfo.setSubjectHeaderInfo(subjectHeaderInfo );
		
		HashMap<RecipientWrapperInfo, LanguageAndTemplateParamWrapperInfo> toRecipientAndParameters = new HashMap<>();
		RecipientWrapperInfo recipient = new RecipientWrapperInfo();
		recipient.setRecipient(this.emailAddress);	// NOte we using the actual email address and not Univ Number, as email not yet saved
		//
		LanguageAndTemplateParamWrapperInfo replacementValuesx = new LanguageAndTemplateParamWrapperInfo();
		replacementValuesx.setLanguageTypeKey(this.lookupUserCorLang);
		HashMap<ParameterNameWrapperInfo, ParameterValueWrapperInfo> messageCatTemplateParameters = new HashMap<>();

		for (Map.Entry<String, String> item : replacementValues.entrySet()) {
		
            if(Strings.isNullOrEmpty(item.getKey()) || Strings.isNullOrEmpty(item.getValue())) {
                continue;
            }

    		ParameterNameWrapperInfo parm = new ParameterNameWrapperInfo();
    		parm.setParameterName(item.getKey());
    		ParameterValueWrapperInfo parmValue = new ParameterValueWrapperInfo();
    		parmValue.setParameterValue(item.getValue());
    		messageCatTemplateParameters.put(parm,parmValue);
        }

		//
		replacementValuesx.setMessageCatTemplateParameters(messageCatTemplateParameters );
		toRecipientAndParameters.put(recipient, replacementValuesx);
		
		emailInfo.setToRecipientAndParameters(toRecipientAndParameters );
		notificationInfo.setEmailInfo(emailInfo);
    	
    	String result = notificationService.sendNotification(notificationInfo);
    	System.out.println("EMAIL : " + result);
		return result;
	}
    
    
//    public void sendEmailSOA(String currentUniversityNumber) {
//        HashMap<String, Object> responseParams = new HashMap<>();
//
//        HashMap<String, String> replacements = new HashMap<>();
//        replacements.put("OTP", otpEmailInfo.getPassword());
//
//        // For logging
//        log.info("Looping over the parameter : templateReplacementValues for SOANotificationWIH: ");
//        if (replacements != null) {
//            for (Map.Entry<String, String> paramItem : replacements.entrySet()) {
//                log.info("replacementParameter : " + paramItem.getKey() + " - " + paramItem.getValue());
//            }
//        }
//        MailType mail = new MailType();
//        mail.setEmailTemplateKeyBody("cd.otp.email.body");
//        mail.setEmailTemplateKeySubject("cd.otp.email.subject");
//        mail.setFromAccountName("DIY-do-not-reply@nwu.ac.za");
//        mail.setFromUserName("DIY-do-not-reply");
//        mail.setToRecipientList(currentUniversityNumber);
//        mail.setReplyToAddress("DIY-do-not-reply@nwu.ac.za");
//        mail.setTemplateReplacementValues(replacements);
//        mail.setConfigurationKey("CONTACT-DETAILS");
//
//        String serviceLookupKey = applicationConfig.getEnvironment().toUpperCase() + "/SOA_NOTIFICATIONSERVICE/" + "V1";
//        log.info("SOASendnotificationService lookupkey : " + serviceLookupKey);
//
//        SOASendnotificationService serviceInstance = null;
//        try {
//            SOASendNotificationServiceClientFactory factory = new SOASendNotificationServiceClientFactory();
//            serviceInstance = factory.getSOASendnotificationService(serviceLookupKey, applicationConfig.getSOA_NOTIFICATION_USERNAME(), applicationConfig.getSOA_NOTIFICATION_PASSWORD());
//            try {
//                MailResponse sendNotificationResponse = serviceInstance.sendNotifications(mail);
//                responseParams.put(SUCCESS, sendNotificationResponse.isSuccessflag());
//            } catch (OperationFailedException e) {
//                log.error("Could not send notification: " + e.getMessage(), e);
//                responseParams.put(FAILIURE, "Failed to send email " + e.getMessage());
//                throw new VaadinUIException("Failed to send email (possible invalid address)",
//                        VaadinUIException.SeverityType.ERROR,
//                        VaadinUIException.Type.CLIENTSIDE);
//            }
//
//        } catch (Exception e1) {
//            log.error("Could not get instance of SOASendnotificationService " + e1.getMessage(), e1);
//            responseParams.put(FAILIURE, "Could not get instance of SOASendnotificationService " + e1.getMessage());
//            throw new VaadinUIException("Failed to send email",
//                    VaadinUIException.SeverityType.ERROR,
//                    VaadinUIException.Type.CLIENTSIDE);
//        }
//    }
}



