package nwu.ac.za.ui.vaadin.impl;

import com.vaadin.ui.*;
import nwu.ac.za.ui.vaadin.ContactDetailStudentAfr;
import nwu.ac.za.ui.vaadin.infc.ContactDetailStudentInfc;

public class ContactDetailsStudentAfrViewImpl extends ContactDetailStudentAfr implements ContactDetailStudentInfc {

	@Override
	public VerticalLayout getVerticalLayout() {
		return this.verticalLayout;
	}

	@Override
	public TextField getOutputUniversityNumber() {
		return this.outputUniversityNumber;
	}

	@Override
	public Label getOutputUniversityNumberLabel() {
		return outputUniversityNumberLabel;
	}

	@Override
	public Label getOutputTitleInitialsSurname() {
		return this.outputTitleInitialsSurname;
	}

	@Override
	public ComboBox getCellphoneCountryCode() {
		return this.cellphoneCountryCode;
	}

	@Override
	public TextField getCellphoneNumber() {
		return this.cellphoneNumber;
	}

	@Override
	public HorizontalLayout getHorizontalLayoutWorkNumber() {
		return horizontalLayoutWorkNumber;
	}

	@Override
	public ComboBox getWorkPhoneCountryCode() {
		return this.workPhoneCountryCode;
	}

	@Override
	public TextField getWorkPhoneNumber() {
		return this.workPhoneNumber;
	}

	@Override
	public ComboBox getHomePhoneCountryCode() {
		return this.homePhoneCountryCode;
	}

	@Override
	public TextField getHomePhoneNumber() {
		return this.homePhoneNumber;
	}

	@Override
	public HorizontalLayout getHorizontalLayoutAccountNumber() {
		return horizontalLayoutAccountNumber;
	}

	@Override
	public Label getAccountNumber() {
		return accountNumber;
	}

	@Override
	public ComboBox getAccountPhoneCountryCode() {
		return accountPhoneCountryCode;
	}

	@Override
	public TextField getAccountPhoneNumber() {
		return accountPhoneNumber;
	}

	@Override
	public TextField geteMailStudent() {
		return this.eMailStudent;
	}

	@Override
	public TextField geteMailStudentConfirm() {
		return this.eMailStudentConfirm;
	}

	@Override
	public TextField geteMailAccount() {
		return this.eMailAccount;
	}

	@Override
	public TextField geteMailAccountConfirm() {
		return this.eMailAccountConfirm;
	}

	@Override
	public Label getUnivNumber() {
		return this.univNumber;
	}

	@Override
	public Label getCellNumber() {
		return this.cellNumber;
	}

	@Override
	public Label getWorkNumber() {
		return this.workNumber;
	}

	@Override
	public Label getHomeNumber() {
		return this.homeNumber;
	}

	@Override
	public Label geteMailAddress() {
		return this.eMailAddress;
	}

	@Override
	public HorizontalLayout getHorizontalLayoutConfirmEmail() {
		return this.horizontalLayoutConfirmEmail;
	}

	@Override
	public Label getConfirmEMailAddress() {
		return this.confirmEMailAddress;
	}

	@Override
	public Label getAccountEMail() {
		return this.accountEMail;
	}

	@Override
	public HorizontalLayout getHorizontalLayoutConfirmAccountEmail() {
		return this.horizontalLayoutConfirmAccountEmail;
	}

	@Override
	public Label getConfirmAccountEMail() {
		return this.confirmAccountEMail;
	}

	@Override
	public VerticalLayout getStudentEmailLayout() {
		return this.studentEmailLayout;
	}

	@Override
	public FormLayout getFormLayout() {
		return this.formLayout;
	}

	public Button getButtonRemoveHomePhoneNumber() {
		return buttonRemoveHomePhoneNumber;
	}

	public void setButtonRemoveHomePhoneNumber(Button buttonRemoveHomePhoneNumber) {
		this.buttonRemoveHomePhoneNumber = buttonRemoveHomePhoneNumber;
	}

	@Override
	public Button getButtonRemoveCellPhoneNumber() {
		return this.buttonRemoveCellPhoneNumber;
	}

	@Override
	public Label getAccountTo() {
		return accountTo;
	}

	@Override
	public ComboBox getAccountToCombo() {
		return accountToCombo;
	}
	@Override
	public Label getAccountAddressee() {
		return accountAddressee;
	}

	@Override
	public TextField getAccountAddresseeText() {
		return accountAddresseeText;
	}

	@Override
	public HorizontalLayout getHorizontalAccountTo(){ return horizontalAccountTo;}

	@Override
	public HorizontalLayout getHorizontalAddressee(){ return horizontalAddressee;}

	@Override
	public HorizontalLayout getHomeNumberLayout() {
		return homeNumberLayout;
	}

	@Override
	public HorizontalLayout getDataNumberLayout() {
		return horizontalLayoutDataNumber;
	}

	@Override
	public Label getDataNumberLabel() {
		return dataNumber;
	}

	@Override
	public Button getDataClearButton() {
		return buttonRemoveDataNumber;
	}

	@Override
	public TextField getDataTextField() {
		return dataPhoneNumberTextField;
	}

	@Override
	public ComboBox getDataComboBox() {
		return dataPhoneCountyCode;
	}

	@Override
	public HorizontalLayout getAddressForAccountLayout() {
		return addressForAccountLayout;
	}


}