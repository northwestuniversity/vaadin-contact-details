package nwu.ac.za.ui;

import java.io.IOException;
import java.util.*;

import javax.servlet.annotation.WebServlet;

import com.vaadin.shared.Position;
import com.vaadin.ui.*;
import nwu.ac.za.service.HREmployeeInformationProxyService;
import nwu.ac.za.util.ContactDetailMessages;
import org.apache.commons.lang.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Property;
import com.vaadin.server.ClassResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;

import ac.za.nwu.common.dto.MetaInfo;
import ac.za.nwu.core.contact.dto.EntityPhoneNumberInfo;
import ac.za.nwu.core.contact.dto.EntityVirtualAddressInfo;
import ac.za.nwu.core.contact.service.ContactService;
import ac.za.nwu.core.contact.service.ContactServiceCRUD;
import ac.za.nwu.core.contact.service.factory.ContactServiceCRUDClientFactory;
import ac.za.nwu.core.contact.service.factory.ContactServiceClientFactory;
import ac.za.nwu.core.person.dto.PersonAffiliationInfo;
import ac.za.nwu.core.person.dto.PersonBiographicInfo;
import ac.za.nwu.core.person.dto.PersonPreferenceInfo;
import ac.za.nwu.core.type.dto.TypeInfo;
import ac.za.nwu.core.type.dto.TypeNameInfo;
import ac.za.nwu.core.type.service.TypeService;
import ac.za.nwu.core.type.service.factory.TypeServiceClientFactory;
import ac.za.nwu.utility.EncryptorUtility;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import assemble.edu.exceptions.VersionMismatchException;
import client.employeeinformation.EmployeeAssignmentDetails;
import client.employeeinformation.EmployeeDetailRequest;
import nwu.ac.za.CountryInformationServiceClientFactory;
import nwu.ac.za.config.ApplicationConfig;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.SinglePageCRUDUI;
import nwu.ac.za.framework.VaadinUIUtility;
import nwu.ac.za.service.PersonProxyService;
import nwu.ac.za.service.StudentProxyService;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException.SeverityType;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException.Type;
import nwu.ac.za.ui.vaadin.impl.ContactDetailsStudentAfrViewImpl;
import nwu.ac.za.ui.vaadin.impl.ContactDetailsStudentEngViewImpl;
import nwu.ac.za.ui.vaadin.infc.ContactDetailStudentInfc;
import nwu.ac.za.util.ContactDetailsConstants;
import nwu.ac.za.util.PropertyValueRetriever;

/**
 * This UI is the application entry point. A UI may either represent a browser
 * window (or tab) or some part of a html page where a Vaadin application is
 * embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is
 * intended to be overridden to add component to the user interface and
 * initialize non-component functionality.
 */
@SuppressWarnings("serial")
@Theme("contact-theme")
public class ContactSPCrudUI extends SinglePageCRUDUI {

	private static final String ERROR_HTTP_RESPONSE_401_UNAUTHORIZED_WHEN_COMMUNICATING = "HTTP response '401: Unauthorized' when communicating with";
	private static final String CONFIG_PROPERTIES_FILENAME = "config.properties";
	private static final String MSG_UNKNOWN = "Unknown ";
	private static final String SAVE_UNSUCCESSFUL = "Save unsuccessful";
	private static final String SAVE_SUCCESSFUL = "Save successful";
	private static final String PROPERTY_NAME_IDENTITY_API_VERSION = "identity.api.version";
	private static final String LANGUAGE_AF = "af";
	private PersonProxyService personProxyService;
	private TypeService typeService;
	private ContextInfo readContextInfo = new ContextInfo(ContactDetailsConstants.APP_NAME_CONTACT_DETAILS);

	private PersonBiographicInfo personBiographicInfo;
	PersonPreferenceInfo personExternalReferenceInfo;
	List<TypeInfo> accToWhomTypeKeys;
	private final String ACCTOWHOM_TypeKey = "vss.code.ACCTOWHOM";
	private final Logger log = LoggerFactory.getLogger(ContactSPCrudUI.class.getName());

	private StudentProxyService studentProxyService;

	private ContactService contactService;
	private ContactServiceCRUD contactServiceCRUD;

	private CountryInformationServiceClientFactory countryInformationService;

	private HREmployeeInformationProxyService hrEmployeeInformationProxyService;
	private EmployeeDetailRequest employeeDetailRequest;
	private EmployeeAssignmentDetails employeeAssignmentDetails;
	private List<EmployeeAssignmentDetails.EmployeeRecord> employeeRecordList;


	private Boolean doSave = true;
	private Boolean mapDTOtoUI = false;
	private Boolean isFirstTime = true;// Dont want the registered events to
										// fire
	private PropertyValueRetriever propertyValueRetriever = new PropertyValueRetriever();

	private Map<String, String> countryPhoneCodes;
	private Map<String, String> countryPhoneCodesDescription;

	private ContactDetailStudentInfc contentViewDetail;
	private boolean isPostGradStudent;
	private String wsHRUser;
	private String wsHRPassword;
	private boolean hasStudentRole;
	private boolean hasEmployeeRole;
	private boolean hasExternalRole;
	private String systemLanguageTypeKey;
	private final String personalPreferenceTypeKeyFather = "vss.code.ACCTOWHOM.1";
	private final String personalPreferenceTypeKeyMother = "vss.code.ACCTOWHOM.2";
	private final String personalPreferenceTypeKeyGuardian = "vss.code.ACCTOWHOM.3";
	private Boolean uiReset = false;
	private ApplicationConfig applicationConfig;
	boolean hasUppercase = false;

	// define security keys and initVector
	public static final String SECKEY = "Bar12345Bar12345";
	public static final String INITVECTOR = "RandomInitVector";

	private EntityPhoneNumberInfo cellPhoneNumber = null;
	private EntityPhoneNumberInfo workPhoneNumber = null;
	private EntityPhoneNumberInfo homePhoneNumber = null;
	private EntityPhoneNumberInfo studentAccountPhoneNumber = null;
	private EntityPhoneNumberInfo dataPhoneNumber = null;
	private EntityVirtualAddressInfo emailVirtualAddressInfo = null;
	private EntityVirtualAddressInfo accEmailVirtualAddressInfo = null;

	private EntityPhoneNumberInfo cellPhoneNumberToSave = null;
	private EntityPhoneNumberInfo workPhoneNumberToSave = null;
	private EntityPhoneNumberInfo homePhoneNumberToSave = null;
	private EntityPhoneNumberInfo studentAccountPhoneNumberToSave = null;
	private EntityPhoneNumberInfo dataPhoneNumberToSave = null;
	private EntityVirtualAddressInfo emailVirtualAddressInfoToSave = null;
	private EntityVirtualAddressInfo accEmailVirtualAddressInfoToSave = null;

	private String isCellNumberChanged;
	private String isWorkNumberChanged;
	private String isHomeNumberChanged;
	private String isStudAccNumberChanged;
	private String isDataNumberChanged;
	private ObjectWriter objWriter;
	protected String isEmailChanged;
	protected String isStudentAccEmailChanged;
	private String lookUserFromUrl;

	private OTPDialog otpDialog;
	private String lookupUserCorLang;

	private static final String CHANGE_CREATE = "CREATE";
	private static final String CHANGE_UPDATE = "UPDATE";
	private static final String CHANGE_DELETE = "DELETE";

	@Override
	public String getBrowserTabTitle() {
		return "Contact Details App";
	}

	@Override
	protected void registerEvents() {

	}

	@Override
	public void registerSinglePageEvents() {
		this.contentViewDetail.getOutputUniversityNumber().addValueChangeListener(new Property.ValueChangeListener() {

			@Override
			public void valueChange(Property.ValueChangeEvent event) {

				if (!mapDTOtoUI) {
					loadDataAndResetData((String) event.getProperty().getValue(), true);

				} else if(isFirstTime) {
					loadDataAndResetData((String) event.getProperty().getValue(), false);
					isFirstTime = false;
				}
			}
		});

		this.contentViewDetail.getCellphoneCountryCode().addValueChangeListener(new Property.ValueChangeListener() {

			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				if (!mapDTOtoUI) {
					String changedCellPhoneCountryCode = getShortDialCode(contentViewDetail.getCellphoneCountryCode());
					isCellNumberChanged = determineCountryDialCodeUpdateOrCreate(changedCellPhoneCountryCode,
							cellPhoneNumber);

					resetValidationMessagesOnComboBox(contentViewDetail.getCellphoneCountryCode());
					validate();
				}

			}

		});

		this.contentViewDetail.getCellphoneNumber().addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				if (!mapDTOtoUI) {
					String changedPhoneNumber = event.getProperty().getValue().toString();
					isCellNumberChanged = determinePhoneNumberUpdateOrCreate(changedPhoneNumber, cellPhoneNumber);

					resetValidationMessageOnTextField(contentViewDetail.getCellphoneNumber());
					validate();
				}
			}

		});

		this.contentViewDetail.getWorkPhoneCountryCode().addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				if (!mapDTOtoUI) {
					String changedCountryCode = getShortDialCode(contentViewDetail.getWorkPhoneCountryCode());
					isWorkNumberChanged = determineCountryDialCodeUpdateOrCreate(changedCountryCode, workPhoneNumber);
					resetValidationMessagesOnComboBox(contentViewDetail.getWorkPhoneCountryCode());
					validate();
				}
			}
		});

		this.contentViewDetail.getWorkPhoneNumber().addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				if (!mapDTOtoUI) {
					String changedPhoneNumber = event.getProperty().getValue().toString();
					isWorkNumberChanged = determinePhoneNumberUpdateOrCreate(changedPhoneNumber, workPhoneNumber);

					resetValidationMessageOnTextField(contentViewDetail.getWorkPhoneNumber());
					validate();
				}
			}
		});

		this.contentViewDetail.getHomePhoneCountryCode().addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				if (!mapDTOtoUI) {
					String changedCountryCode = getShortDialCode(contentViewDetail.getHomePhoneCountryCode());
					isHomeNumberChanged = determineCountryDialCodeUpdateOrCreate(changedCountryCode, homePhoneNumber);
					resetValidationMessagesOnComboBox(contentViewDetail.getHomePhoneCountryCode());
					validate();
				}

			}
		});

		this.contentViewDetail.getHomePhoneNumber().addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				if (!mapDTOtoUI) {
					String changedPhoneNumber = event.getProperty().getValue().toString();
					isHomeNumberChanged = determinePhoneNumberUpdateOrCreate(changedPhoneNumber, homePhoneNumber);
					resetValidationMessageOnTextField(contentViewDetail.getHomePhoneNumber());
					validate();
				}
			}
		});

		this.contentViewDetail.getAccountPhoneCountryCode().addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				if (!mapDTOtoUI) {
					String changedCountryCode = getShortDialCode(contentViewDetail.getAccountPhoneCountryCode());
					isStudAccNumberChanged = determineCountryDialCodeUpdateOrCreate(changedCountryCode,
							studentAccountPhoneNumber);
					resetValidationMessagesOnComboBox(contentViewDetail.getAccountPhoneCountryCode());
					validate();
				}
			}
		});

		this.contentViewDetail.getAccountPhoneNumber().addValueChangeListener(new Property.ValueChangeListener() {
			@Override

			public void valueChange(Property.ValueChangeEvent event) {
				if (!mapDTOtoUI) {
					String changedPhoneNumber = event.getProperty().getValue().toString();
					isStudAccNumberChanged = determinePhoneNumberUpdateOrCreate(changedPhoneNumber,
							studentAccountPhoneNumber);
					resetValidationMessageOnTextField(contentViewDetail.getAccountPhoneNumber());
					validate();
				}
			}
		});

		contentViewDetail.geteMailStudent().addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				if (!mapDTOtoUI) {
					String changedEmail = (String) event.getProperty().getValue();
					if (changedEmail != null) {
						resetValidationMessageOnTextField(contentViewDetail.geteMailStudent()); // Nina
						isEmailChanged = determineEmailUpdateOrCreate(changedEmail, emailVirtualAddressInfo);
						if (isEmailChanged != null) {
							showHideEmailConfirmField(true);
						} else {
							showHideEmailConfirmField(false);
						}
					}
				}
			}

		});

		this.contentViewDetail.geteMailStudentConfirm().addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				if (!mapDTOtoUI) {
					resetValidationMessageOnTextField(contentViewDetail.geteMailStudent());
				}
			}
		});

		this.contentViewDetail.geteMailAccount().addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				if (!mapDTOtoUI) {
					String changedStudentAccEmail = (String) event.getProperty().getValue();
					if (changedStudentAccEmail != null) {
						resetValidationMessageOnTextField(contentViewDetail.geteMailAccount()); // Nina
						isStudentAccEmailChanged = determineEmailUpdateOrCreate(changedStudentAccEmail,
								accEmailVirtualAddressInfo);

						if (isStudentAccEmailChanged != null) {
							showHideEmailAccountConfirmField(true);
						} else {
							showHideEmailAccountConfirmField(true);
						}
					}
				}
			}
		});

		this.contentViewDetail.geteMailAccountConfirm().addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				if (!mapDTOtoUI) {
					resetValidationMessageOnTextField(contentViewDetail.geteMailAccount());
					resetValidationMessageOnTextField(contentViewDetail.geteMailAccountConfirm());
					validate();
				}
			}
		});

		this.contentViewDetail.getDataComboBox().addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				if (!mapDTOtoUI) {
					String changedCountryCode = getShortDialCode(contentViewDetail.getDataComboBox());
					isDataNumberChanged = determineCountryDialCodeUpdateOrCreate(changedCountryCode,
							dataPhoneNumber);
					resetValidationMessagesOnComboBox(contentViewDetail.getDataComboBox());
					validate();
				}
			}
		});

		this.contentViewDetail.getDataTextField().addValueChangeListener(new Property.ValueChangeListener() {
			@Override

			public void valueChange(Property.ValueChangeEvent event) {
				if (!mapDTOtoUI) {
					String changedPhoneNumber = event.getProperty().getValue().toString();
					isDataNumberChanged = determinePhoneNumberUpdateOrCreate(changedPhoneNumber,
							dataPhoneNumber);
					resetValidationMessageOnTextField(contentViewDetail.getDataTextField());
					validate();
				}
			}
		});

		// Remove Data Number Button Event
		this.contentViewDetail.getDataClearButton().addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(Button.ClickEvent event) {
				mapDTOtoUI = true;
				contentViewDetail.getDataComboBox().setValue(null);
				contentViewDetail.getDataComboBox().setRequired(false);

				contentViewDetail.getDataTextField().setNullRepresentation("");
				contentViewDetail.getDataTextField().setValue(null);

				isDataNumberChanged = CHANGE_DELETE;

				mapDTOtoUI = false;
			}
		});

		// Remove Home Number Button Event
		this.contentViewDetail.getButtonRemoveHomePhoneNumber().addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(Button.ClickEvent event) {
				mapDTOtoUI = true;
				contentViewDetail.getHomePhoneCountryCode().setValue(null);
				contentViewDetail.getHomePhoneCountryCode().setRequired(false);

				contentViewDetail.getHomePhoneNumber().setNullRepresentation("");
				contentViewDetail.getHomePhoneNumber().setValue(null);

				isHomeNumberChanged = CHANGE_DELETE;

				mapDTOtoUI = false;
			}
		});

		// Remove Home Number Button Event
		this.contentViewDetail.getButtonRemoveCellPhoneNumber().addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(Button.ClickEvent event) {
				mapDTOtoUI = true;
				contentViewDetail.getCellphoneCountryCode().setValue(null);

				contentViewDetail.getCellphoneNumber().setNullRepresentation("");
				contentViewDetail.getCellphoneNumber().setValue(null);

				isCellNumberChanged = CHANGE_DELETE;
				mapDTOtoUI = false;
			}
		});

	}

	private void loadDataAndResetData(String universityNumber, boolean ignoreURL) {
		String value = universityNumber;

		if(lookUserFromUrl != null && !lookUserFromUrl.isEmpty() && !ignoreURL) {
			value = lookUserFromUrl;
		}

		if (value != null) {
			resetUI();
			lookupUser = value;
			loadInitialData();
		}
	}

	public void validate() {
		if (!uiReset) {
			beforeSaveValidations();
		}
	}

	public void setRequiredDialAndPhoneHome(ComboBox dialCode, TextField phoneNumber) {
		dialCode.setRequired(false);
		phoneNumber.setRequired(false);

		if (dialCode.getValue() == null && (phoneNumber.getValue() == null || phoneNumber.getValue().length() == 0)) {
			dialCode.setComponentError(null);
			dialCode.setValidationVisible(false);
			dialCode.setRequired(false);
			phoneNumber.setComponentError(null);
			phoneNumber.setValidationVisible(false);
			phoneNumber.setRequired(false);
			phoneNumber.setValue(null);
		}

		if (dialCode.getValue() == null && phoneNumber.getValue() != null) {
			// Can't make a field required if it is read only
			if (!dialCode.isReadOnly()) {
				dialCode.setRequired(true);
			}
		}

		if (phoneNumber.getValue() == null && dialCode.getValue() != null) {
			phoneNumber.setRequired(true);
		} else if (phoneNumber.getValue() != null) {
			if (phoneNumber.getValue().isEmpty() && dialCode.getValue() != null) {
				phoneNumber.setRequired(true);
			}
		}
	}

	public void setRequiredDialAndPhone(ComboBox dialCode, TextField phoneNumber, Boolean create) {
		if (create) {
			if (dialCode.getValue() == null
					&& (phoneNumber.getValue() == null || phoneNumber.getValue().length() == 0)) {
				dialCode.setComponentError(null);
				dialCode.setValidationVisible(false);
				dialCode.setRequired(false);
				phoneNumber.setComponentError(null);
				phoneNumber.setValidationVisible(false);
				phoneNumber.setRequired(false);
				phoneNumber.setValue(null);
			} else if (dialCode.getValue() == null && phoneNumber.getValue() != null) {
				dialCode.setRequired(true);
			} else if (phoneNumber.getValue() == null && dialCode.getValue() != null) {
				phoneNumber.setRequired(true);
			} else if (phoneNumber.getValue() != null) {
				if (phoneNumber.getValue().isEmpty() && dialCode.getValue() != null) {
					phoneNumber.setRequired(true);
				}
			}
		}
	}

	public void setRequiredDialAndPhone(ComboBox dialCode, TextField phoneNumber) {

		dialCode.setRequired(false);
		phoneNumber.setRequired(false);

		if (dialCode.getValue() == null && (phoneNumber.getValue() == null || phoneNumber.getValue().length() == 0)
				&& !hasStudentRole) { 
			dialCode.setComponentError(null);
			dialCode.setValidationVisible(false);
			dialCode.setRequired(false);
			phoneNumber.setComponentError(null);
			phoneNumber.setValidationVisible(false);
			phoneNumber.setRequired(false);
			phoneNumber.setValue(null);
		} else if (dialCode.getValue() == null && phoneNumber.getValue() != null) {
			dialCode.setRequired(true);
		} else if (phoneNumber.getValue() == null && dialCode.getValue() != null) {
			phoneNumber.setRequired(true);
		} else if (phoneNumber.getValue() != null) {
			if (phoneNumber.getValue().isEmpty() && dialCode.getValue() != null) {
				phoneNumber.setRequired(true);
			}
		}
	}

	public void showHideEmailConfirmField(Boolean show) {
		contentViewDetail.geteMailStudentConfirm().setRequired(show);
		contentViewDetail.geteMailStudentConfirm().setValue(null);

		if (show) {
			if (contentViewDetail.getStudentEmailLayout()
					.getComponentIndex(contentViewDetail.getHorizontalLayoutConfirmEmail()) == -1) {
				if (!contentViewDetail.getStudentEmailLayout().isVisible()) {
					contentViewDetail.getStudentEmailLayout().setVisible(true);
				}
				contentViewDetail.getStudentEmailLayout()
						.addComponent(contentViewDetail.getHorizontalLayoutConfirmEmail(), 0);
			}
		} else {
			if (contentViewDetail.getStudentEmailLayout()
					.getComponentIndex(contentViewDetail.getHorizontalLayoutConfirmEmail()) != -1) {
				contentViewDetail.getStudentEmailLayout()
						.removeComponent(contentViewDetail.getHorizontalLayoutConfirmEmail());
			}
		}
	}

	public void showHideEmailAccountConfirmField(Boolean show) {
		contentViewDetail.geteMailAccountConfirm().setRequired(show);
		contentViewDetail.geteMailAccountConfirm().setNullRepresentation("");		
		contentViewDetail.geteMailAccountConfirm().setValue(null);

		if (show) {
			if (contentViewDetail.getStudentEmailLayout()
					.getComponentIndex(contentViewDetail.getHorizontalLayoutConfirmAccountEmail()) == -1) {
				contentViewDetail.getStudentEmailLayout()
						.addComponent(contentViewDetail.getHorizontalLayoutConfirmAccountEmail());
			}
		} else {
			if (contentViewDetail.getStudentEmailLayout()
					.getComponentIndex(contentViewDetail.getHorizontalLayoutConfirmAccountEmail()) != -1) {
				contentViewDetail.getStudentEmailLayout()
						.removeComponent(contentViewDetail.getHorizontalLayoutConfirmAccountEmail());
			}
		}
	}

	public void setupPersonRoles() {
		hasStudentRole = false;
		hasEmployeeRole = false;
		hasExternalRole = false;


		List<PersonAffiliationInfo> personAffiliationInfos = personProxyService.getPersonAffiliation(lookupUser, null);

		if (personAffiliationInfos != null) {

			for (PersonAffiliationInfo personAffiliationInfo : personAffiliationInfos) {
				// Check for Student Role
				if (personAffiliationInfo.getAffiliationTypeKey()
						.contains(ContactDetailsConstants.TYPE_KEY_PREFIX + "."
								+ ContactDetailsConstants.AFFILIATION_INDEX + "."
								+ ContactDetailsConstants.PersonAffiliationTypes.EMPLOYEE_CODE_INDEX)) {
					hasEmployeeRole = true;

				} else if (personAffiliationInfo.getAffiliationTypeKey().equalsIgnoreCase(
						ContactDetailsConstants.TYPE_KEY_PREFIX + "." + ContactDetailsConstants.AFFILIATION_INDEX + "."
								+ ContactDetailsConstants.PersonAffiliationTypes.EXTERNAL_CODE_INDEX + "."
								+ ContactDetailsConstants.PersonAffiliationTypes.EXTERNAL_TYPE)) {
					hasExternalRole = true;
				}
				if (personAffiliationInfo.getAffiliationTypeKey().equalsIgnoreCase(
						ContactDetailsConstants.TYPE_KEY_PREFIX + "." + ContactDetailsConstants.AFFILIATION_INDEX + "."
								+ ContactDetailsConstants.PersonAffiliationTypes.EXTERNAL_CODE_INDEX + "."
								+ ContactDetailsConstants.PersonAffiliationTypes.EXTERNAL_TYPE_PERSON_E)) {
					hasExternalRole = true;
				}
				if (personAffiliationInfo.getAffiliationTypeKey()
						.equalsIgnoreCase(ContactDetailsConstants.TYPE_KEY_PREFIX + "."
								+ ContactDetailsConstants.AFFILIATION_INDEX + "."
								+ ContactDetailsConstants.PersonAffiliationTypes.STUDENT_CODE_INDEX + "."
								+ ContactDetailsConstants.PersonAffiliationTypes.LEARNER_TYPE)) {
					// TMC-168 hasStudentRole = true;
					
					// Confirm that it is a active student...
					hasStudentRole = studentProxyService.returningStudentOrApprovedApplicant(lookupUser);
					
				}
			}
		}
	}

	@Override
	protected void serviceInitialization() {
		applicationConfig = ApplicationConfig.getInstance(appPropertyFileName);
		personProxyService = new PersonProxyService(applicationConfig);

		try {
			String typeServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
					TypeServiceClientFactory.TYPESERVICE, PROPERTY_NAME_IDENTITY_API_VERSION, null,
					this.appPropertyFileName, this.getAppRuntimeEnvironment());

			String pws = applicationConfig.getWsIAPIReadPassword();
			String decryptedPWS = EncryptorUtility.decrypt(SECKEY, INITVECTOR, pws);

			String encryptedPWS = EncryptorUtility.encrypt(SECKEY, INITVECTOR, pws);

			typeService = TypeServiceClientFactory.getTypeService(typeServiceLookupKey,
					applicationConfig.getIAPIReadUser(), decryptedPWS);

		} catch (Exception e) {
			throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSTARTUPFAILURE, false);
		}

		studentProxyService = new StudentProxyService(applicationConfig, appPropertyFileName);
		try {
			String contactServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
					ContactServiceClientFactory.CONTACTSERVICE, PROPERTY_NAME_IDENTITY_API_VERSION, null,
					this.appPropertyFileName, this.getAppRuntimeEnvironment());

			String pwsIAPA = applicationConfig.getWsIAPIReadPassword();

			String decryptedPWS = EncryptorUtility.decrypt(SECKEY, INITVECTOR, pwsIAPA);
			contactService = ContactServiceClientFactory.getContactService(contactServiceLookupKey,
					applicationConfig.getIAPIReadUser(), decryptedPWS);
		} catch (Exception e) {
			throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSTARTUPFAILURE, false);
		}

		try {
			String contactCRUDServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
					ContactServiceCRUDClientFactory.CONTACTSERVICECRUD, PROPERTY_NAME_IDENTITY_API_VERSION, null,
					this.appPropertyFileName, this.getAppRuntimeEnvironment());

			String pwsIAPACRUD = applicationConfig.getAPICRUDPassword();

			String decryptedPWS = EncryptorUtility.decrypt(SECKEY, INITVECTOR, pwsIAPACRUD);
			contactServiceCRUD = ContactServiceCRUDClientFactory.getContactServiceCRUD(contactCRUDServiceLookupKey,
					applicationConfig.getAPICRUDUserName(), decryptedPWS);
		} catch (Exception e) {
			throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSTARTUPFAILURE, false);
		}

		hrEmployeeInformationProxyService = new HREmployeeInformationProxyService(getAppRuntimeEnvironment(),
				wsHRPassword, wsHRUser, appPropertyFileName);

		try {
			countryInformationService = new CountryInformationServiceClientFactory();
		} catch (Exception e) {
			throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSTARTUPFAILURE, false);
		}
	}

	@WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = ContactSPCrudUI.class, productionMode = false)
	public static class MyUIServlet extends VaadinServlet {
	}

	public void setDetailView() {
		String lang = UI.getCurrent().getLocale().getLanguage();
		VerticalLayout view;

		if (lang.equals(LANGUAGE_AF)) {
			view = new ContactDetailsStudentAfrViewImpl();

		} else {
			view = new ContactDetailsStudentEngViewImpl();

		}
		this.detailContent = view;
		contentViewDetail = (ContactDetailStudentInfc) this.detailContent;
	}

	@Override
	public void afterUIInitialization() {
		this.contentViewDetail.getCellphoneCountryCode().setNewItemsAllowed(false);
		this.contentViewDetail.getCellphoneCountryCode().setNullSelectionAllowed(true);
		this.contentViewDetail.getWorkPhoneCountryCode().setNewItemsAllowed(false);
		this.contentViewDetail.getWorkPhoneCountryCode().setNullSelectionAllowed(true);
		this.contentViewDetail.getHomePhoneCountryCode().setNewItemsAllowed(false);
		this.contentViewDetail.getHomePhoneCountryCode().setNullSelectionAllowed(true);
		this.contentViewDetail.getAccountPhoneCountryCode().setNewItemsAllowed(false);
		this.contentViewDetail.getAccountPhoneCountryCode().setNullSelectionAllowed(true);
		this.contentViewDetail.getDataComboBox().setNewItemsAllowed(false);
		this.contentViewDetail.getDataComboBox().setNullSelectionAllowed(true);

		populateUIComboComponents();
		loadInitialData();
	}

	public void setReadOnlyFields() {
		setAccountVisibility(false);

		// Univ input readonly unless Allow univ number input is configured true
		try {
			if (propertyValueRetriever.getPropertyValues(ContactDetailsConstants.ALLOW_UNIV_NUMBER_INPUT,
					ContactDetailsConstants.PROPERTY_FILE_NAME).equals("false")) {
				this.contentViewDetail.getOutputUniversityNumber().setReadOnly(true);
				this.contentViewDetail.getOutputUniversityNumber().setVisible(false);
				this.contentViewDetail.getOutputUniversityNumberLabel().setVisible(true);
			}
		} catch (IOException e) {
			throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSTARTUPFAILURE, false);
		}

		if (employeeRecordList != null && employeeRecordList.size() != 0) {
			String personType = employeeRecordList.get(0).getPersonType();
			if (personType.equalsIgnoreCase(ContactDetailsConstants.HRAppointmentTypes.PERMANENT)
					|| personType.equalsIgnoreCase(ContactDetailsConstants.HRAppointmentTypes.FIXED_TERM)
					|| personType.equalsIgnoreCase(ContactDetailsConstants.HRAppointmentTypes.TEMPORARY)
					|| personType.equalsIgnoreCase(ContactDetailsConstants.HRAppointmentTypes.TEMP_FIXED_TERM_)) {
				contentViewDetail.getWorkPhoneNumber().setReadOnly(true);
				contentViewDetail.getWorkPhoneNumber().setRequired(false);
				contentViewDetail.getWorkPhoneCountryCode().setReadOnly(true);

				contentViewDetail.geteMailStudent().setReadOnly(true);
				contentViewDetail.geteMailStudent().setRequired(false);
			}
		} else {
			// TODO TMC-88 Hiding this until backend are ready.
			// addAccountToComponents();

		}
	}

	private String typeKeySubstring(String typeKey) {
		// Extract substring value from TypeKey
		return typeKey.substring(typeKey.lastIndexOf('.') + 1);
	}

	private void addAccountToComponents() {
		setAccountVisibility(true);
		List<TypeNameInfo> toWhomList = new ArrayList<>();
		String toWhom = "";

		if (accToWhomTypeKeys != null && accToWhomTypeKeys.size() > 0) {
			contentViewDetail.getAccountToCombo().setRequired(false);
			for (TypeInfo typeInfo : accToWhomTypeKeys) {
				if (typeInfo.getTypeKey().substring(0, typeInfo.getTypeKey().length() - 1)
						.contains(personalPreferenceTypeKeyFather)
						|| typeInfo.getTypeKey().substring(0, typeInfo.getTypeKey().length() - 1)
								.contains(personalPreferenceTypeKeyMother)
						|| typeInfo.getTypeKey().substring(0, typeInfo.getTypeKey().length() - 1)
								.contains(personalPreferenceTypeKeyGuardian)) {
					toWhom = typeInfo.getCategory();
					toWhomList = typeInfo.getTypeNames();
					for (TypeNameInfo names : toWhomList) {
						toWhom = names.getLongDescr();
						contentViewDetail.getAccountToCombo().addItem(toWhom);
					}
				}
			}
		} else {
			contentViewDetail.getAccountToCombo().setRequired(true);
			contentViewDetail.getAccountToCombo().setRequiredError("Error Address to whom unavailable");
		}

		// TMC-88 Not looking for person preferences until the backend are
		// ready.
		// List<PersonPreferenceInfo> personPreferenceInfos =
		// personProxyService.getPersonPreference(lookupUser, null);
		//
		// if(personPreferenceInfos != null && personPreferenceInfos.size() > 0)
		// {
		// for(PersonPreferenceInfo personInfo:personPreferenceInfos) {
		//
		// contentViewDetail.getAccountToCombo().select(personInfo.preferenceTypeKey);
		// contentViewDetail.getAccountAddresseeText().setValue(personInfo.preferenceValue);
		// }
		// }
	}

	private void setAccountVisibility(Boolean visibility) {
		// TMC-88 Hiding this until backend are ready.
		contentViewDetail.getHorizontalAddressee().setVisible(false);
		contentViewDetail.getHorizontalAccountTo().setVisible(false);
	}

	@Override
	public String getServiceRegistryEnvironmentTypeKey(String serviceName) {
		return null;
	}

	@Override
	public void mapDTOtoUIComponents() {
		log.info("mapDTOtoUIComponents");
		String[] parametersResult = urlParameterMap.get("lookupUser");
		if(parametersResult != null) {
			lookUserFromUrl = String.valueOf(Long.parseLong(parametersResult[0]));
		}

		mapDTOtoUI = true;
		contentViewDetail.getOutputUniversityNumber().setValue(lookupUser);
		contentViewDetail.getOutputUniversityNumberLabel().setValue(lookupUser);
		String titleKey = personBiographicInfo.getTitleTypeKey();
		String title = titleKey.substring(titleKey.lastIndexOf('.') + 1);
		String initials = personBiographicInfo.getInitials();
		String surname = personBiographicInfo.getLastName();
		contentViewDetail.getOutputTitleInitialsSurname().setValue(title + " " + initials + " " + surname);

		try {
			cellPhoneNumber = contactService.getEntityPhoneNumberByPurposeType(lookupUser,
					ContactDetailsConstants.CONTACT_SERVICE_PHONENUMBERS_CELLPHONE, this.readContextInfo);
			setInitialDialCode(contentViewDetail.getCellphoneCountryCode(), cellPhoneNumber.getCountryDailPrefix());
			contentViewDetail.getCellphoneNumber().setValue(cellPhoneNumber.getPhoneNumber());
		} catch (DoesNotExistException e) {
			// Ignore DoesNotExistException as person can create a number
		} catch (InvalidParameterException | OperationFailedException | MissingParameterException
				| PermissionDeniedException e) {
			throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSIDE, false);
		}

		// TMC-170 - Only read work number for relevant roles - coz some students does have work numbers in vss with out the 
		// country dial code - which causes problems coz this app want to validate it then but it's not on the screen
		if (hasEmployeeRole || isPostGradStudent) {
			try {
				workPhoneNumber = contactService.getEntityPhoneNumberByPurposeType(lookupUser,
						ContactDetailsConstants.CONTACT_SERVICE_PHONENUMBERS_WORK, this.readContextInfo);
	
				String dialCode = workPhoneNumber.getCountryDailPrefix();
				setInitialDialCode(contentViewDetail.getWorkPhoneCountryCode(), dialCode);
				//
				contentViewDetail.getWorkPhoneNumber().setValue(workPhoneNumber.getPhoneNumber());
	
			} catch (DoesNotExistException e) {
				// Ignore DoesNotExistException as person can create a number
	
			} catch (InvalidParameterException | OperationFailedException | MissingParameterException
					| PermissionDeniedException e) {
				throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSIDE, false);
			}
		}
		try {
			homePhoneNumber = contactService.getEntityPhoneNumberByPurposeType(lookupUser,
					ContactDetailsConstants.CONTACT_SERVICE_PHONENUMBERS_HOME, this.readContextInfo);

			String dialCode = homePhoneNumber.getCountryDailPrefix();
			setInitialDialCode(contentViewDetail.getHomePhoneCountryCode(), dialCode);
			contentViewDetail.getHomePhoneNumber().setValue(homePhoneNumber.getPhoneNumber());
		} catch (DoesNotExistException e) {
			// Ignore DoesNotExistException as person can create a number
		} catch (InvalidParameterException | OperationFailedException | MissingParameterException
				| PermissionDeniedException e) {
			throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSIDE, false);
		}

		// Only students will have account phone numbers
		if (hasStudentRole || hasExternalRole) {
			try {
				studentAccountPhoneNumber = contactService.getEntityPhoneNumberByPurposeType(lookupUser,
						ContactDetailsConstants.CONTACT_SERVICE_PHONENUMBERS_ACCOUNT, this.readContextInfo);

				String dialCode = studentAccountPhoneNumber.getCountryDailPrefix();
				setInitialDialCode(contentViewDetail.getAccountPhoneCountryCode(), dialCode);
				//
				contentViewDetail.getAccountPhoneNumber().setValue(studentAccountPhoneNumber.getPhoneNumber());
			} catch (DoesNotExistException e) {
				// Ignore DoesNotExistException as person can create a number
			} catch (InvalidParameterException | OperationFailedException | MissingParameterException
					| PermissionDeniedException e) {
				throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSIDE, false);
			}

		}

		if (hasStudentRole || hasEmployeeRole) {
			try {
				dataPhoneNumber = contactService.getEntityPhoneNumberByPurposeType(lookupUser,
						ContactDetailsConstants.CONTACT_SERVICE_PHONENUMBERS_DATA, this.readContextInfo);

				String dialCode = dataPhoneNumber.getCountryDailPrefix();
				setInitialDialCode(contentViewDetail.getDataComboBox(), dialCode);
				//
				contentViewDetail.getDataTextField().setValue(dataPhoneNumber.getPhoneNumber());
			} catch (DoesNotExistException e) {
				// Ignore DoesNotExistException as person can create a number
			} catch (InvalidParameterException | OperationFailedException | MissingParameterException
					| PermissionDeniedException e) {
				throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSIDE, false);
			}

		}


		try {
			List<EntityVirtualAddressInfo> emailVirtualAddresses = contactService.getEntityVirtualAddressByPurposeType(
					lookupUser, ContactDetailsConstants.CONTACT_SERVICE_VIRTUALADDRESS_EMAIL, this.readContextInfo);
			if (emailVirtualAddresses != null && emailVirtualAddresses.size() > 0) {
				emailVirtualAddressInfo = emailVirtualAddresses.get(0);
				// Issue with service not populating the Univ number TIA-343
				if (emailVirtualAddressInfo.getUnivNumber() == null) {
					emailVirtualAddressInfo.setUnivNumber(lookupUser);
				}
				contentViewDetail.geteMailStudent().setValue(emailVirtualAddressInfo.getVirtualAddress());
			}

		} catch (DoesNotExistException e) {
			// Ignore DoesNotExistException as person can create a number
		} catch (InvalidParameterException | OperationFailedException | MissingParameterException
				| PermissionDeniedException e) {
			throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSIDE, false);
		}

		if (hasStudentRole || hasExternalRole) {
			try {
				List<EntityVirtualAddressInfo> accEmailVirtualAddresses = contactService
						.getEntityVirtualAddressByPurposeType(lookupUser,
								ContactDetailsConstants.CONTACT_SERVICE_VIRTUALADDRESS_EMAILACC, this.readContextInfo);

				if (accEmailVirtualAddresses != null && accEmailVirtualAddresses.size() > 0) {
					accEmailVirtualAddressInfo = accEmailVirtualAddresses.get(0);
					// Issue with service not populating the Univ number TIA-343
					if (accEmailVirtualAddressInfo.getUnivNumber() == null) {
						accEmailVirtualAddressInfo.setUnivNumber(lookupUser);
					}
					contentViewDetail.geteMailAccount().setValue(accEmailVirtualAddressInfo.getVirtualAddress());
				}
			} catch (DoesNotExistException e) {
				// Ignore DoesNotExistException as person can create a number
			} catch (InvalidParameterException | OperationFailedException | MissingParameterException
					| PermissionDeniedException e) {
				throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSIDE, false);
			}

		}

		try {
			employeeDetailRequest = new EmployeeDetailRequest();
			employeeDetailRequest.setEmployeeNumber(lookupUser);
			employeeDetailRequest.setEffectiveDate(null);

			employeeAssignmentDetails = hrEmployeeInformationProxyService.getEmployeeDetail(employeeDetailRequest);
			employeeRecordList = employeeAssignmentDetails.getEmployeeRecord();
		} catch (Exception e) {
			throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSIDE, false);
		}

		mapDTOtoUI = false;
	}

	private void setVisibilityOfPanelsBasedOnRoles() {
		contentViewDetail.getHomeNumberLayout().setVisible(false);
		contentViewDetail.getAddressForAccountLayout().setVisible(false);
		contentViewDetail.getStudentEmailLayout().setVisible(false);
		setVisibilityOfAccountNumber(false);
		setVisibilityOfWorkPhoneFields(false);

		if (hasEmployeeRole) {
			contentViewDetail.getHomeNumberLayout().setVisible(true);
			contentViewDetail.getDataNumberLayout().setVisible(true);
			setVisibilityOfWorkPhoneFields(true);
			this.contentViewDetail.getButtonRemoveCellPhoneNumber().setVisible(true);

		}

		if (hasStudentRole) {
			contentViewDetail.getHomeNumberLayout().setVisible(true);
			contentViewDetail.getAddressForAccountLayout().setVisible(true);
			contentViewDetail.getStudentEmailLayout().setVisible(true);
			contentViewDetail.getDataNumberLayout().setVisible(true);
			this.contentViewDetail.getButtonRemoveCellPhoneNumber().setVisible(false);
			setVisibilityOfAccountNumber(true);
		}
		if (hasExternalRole) {
			contentViewDetail.getHomeNumberLayout().setVisible(true);
			contentViewDetail.getStudentEmailLayout().setVisible(true);
			this.contentViewDetail.getButtonRemoveCellPhoneNumber().setVisible(false);
		}
		if (isPostGradStudent) {
			setVisibilityOfWorkPhoneFields(true);
			this.contentViewDetail.getButtonRemoveCellPhoneNumber().setVisible(false);
		}

	}

	private void setVisibilityOfAccountNumber(boolean visibility) {
		contentViewDetail.getAccountNumber().setVisible(visibility);
		contentViewDetail.getAccountPhoneCountryCode().setVisible(visibility);
		contentViewDetail.getAccountPhoneNumber().setVisible(visibility);
		contentViewDetail.getAccountPhoneNumber().getParent().setVisible(visibility);
	}

	@Override
	public void mapUIComponentsToDTO() {
		//
		String uiPhoneNumber = null;
		String uiCountryCode = null;
		//
		if (isCellNumberChanged != null) {
			uiPhoneNumber = contentViewDetail.getCellphoneNumber().getValue();
			uiCountryCode = getShortDialCode(contentViewDetail.getCellphoneCountryCode());
			cellPhoneNumberToSave = setupPhoneNumberToSave(uiPhoneNumber, uiCountryCode, isCellNumberChanged,
					cellPhoneNumber, ContactDetailsConstants.CONTACT_SERVICE_PHONENUMBERS_CELLPHONE);
		}

		if (isWorkNumberChanged != null) {
			uiPhoneNumber = contentViewDetail.getWorkPhoneNumber().getValue();
			uiCountryCode = getShortDialCode(contentViewDetail.getWorkPhoneCountryCode());
			workPhoneNumberToSave = setupPhoneNumberToSave(uiPhoneNumber, uiCountryCode, isWorkNumberChanged,
					workPhoneNumber, ContactDetailsConstants.CONTACT_SERVICE_PHONENUMBERS_WORK);
		}

		if (isHomeNumberChanged != null) {
			uiPhoneNumber = contentViewDetail.getHomePhoneNumber().getValue();
			uiCountryCode = getShortDialCode(contentViewDetail.getHomePhoneCountryCode());
			homePhoneNumberToSave = setupPhoneNumberToSave(uiPhoneNumber, uiCountryCode, isHomeNumberChanged,
					homePhoneNumber, ContactDetailsConstants.CONTACT_SERVICE_PHONENUMBERS_HOME);
		}

		if (isStudAccNumberChanged != null) {
			uiPhoneNumber = contentViewDetail.getAccountPhoneNumber().getValue();
			uiCountryCode = getShortDialCode(contentViewDetail.getAccountPhoneCountryCode());
			studentAccountPhoneNumberToSave = setupPhoneNumberToSave(uiPhoneNumber, uiCountryCode,
					isStudAccNumberChanged, studentAccountPhoneNumber,
					ContactDetailsConstants.CONTACT_SERVICE_PHONENUMBERS_ACCOUNT);
		}

		if(isDataNumberChanged != null) {
			uiPhoneNumber = contentViewDetail.getDataTextField().getValue();
			uiCountryCode = getShortDialCode(contentViewDetail.getDataComboBox());
			dataPhoneNumberToSave = setupPhoneNumberToSave(uiPhoneNumber, uiCountryCode,
					isDataNumberChanged, dataPhoneNumber, ContactDetailsConstants.CONTACT_SERVICE_PHONENUMBERS_DATA);
		}

		if (isEmailChanged != null) {
			emailVirtualAddressInfoToSave = setupEmailToSave(contentViewDetail.geteMailStudent().getValue(), isEmailChanged, emailVirtualAddressInfo, ContactDetailsConstants.CONTACT_SERVICE_VIRTUALADDRESS_EMAIL);
		}
		if (isStudentAccEmailChanged != null) {
			accEmailVirtualAddressInfoToSave = setupEmailToSave(contentViewDetail.geteMailAccount().getValue(), isStudentAccEmailChanged, accEmailVirtualAddressInfo, ContactDetailsConstants.CONTACT_SERVICE_VIRTUALADDRESS_EMAILACC);			
		}
	}

	private EntityPhoneNumberInfo setupPhoneNumberToSave(String uiPhoneNumber, String uiCountryCode,
			String isNumberChanged, EntityPhoneNumberInfo phoneNumberFromDB, String phoneNumberTypeKey) {
		EntityPhoneNumberInfo numberToSave = null;
		if (isNumberChanged.equals(CHANGE_CREATE)) {
			// Construct a new object
			numberToSave = new EntityPhoneNumberInfo();
			MetaInfo metaInfo = new MetaInfo();
			metaInfo.setUpdateTime(new Date());
			metaInfo.setUpdateId(authenticatedUser);
			metaInfo.setAuditFunction(authenticatedUser);
			metaInfo.setCreateId(authenticatedUser);
			numberToSave.setMetaInfo(metaInfo);
			numberToSave.setUnivNumber(lookupUser);
			numberToSave.phoneNumberPurposeTypeKey = phoneNumberTypeKey;
			numberToSave.setPhoneNumber(uiPhoneNumber);
			numberToSave.setCountryDailPrefix(uiCountryCode);
			numberToSave.setStartDate(new Date());
		} else if (isNumberChanged.equals(CHANGE_UPDATE)) {
			if (phoneNumberFromDB != null) {
				// Make a copy of the object that was read from DB before any
				// changes
				numberToSave = (EntityPhoneNumberInfo) SerializationUtils.clone(phoneNumberFromDB);
				numberToSave.setPhoneNumber(uiPhoneNumber);
				numberToSave.setCountryDailPrefix(uiCountryCode);
			} else {
				// TODO NINA
				System.out.println("Something is wrong this scenario should not happen - pls investigate");
				throw new VaadinUIException("Something is wrong this scenario should not happen - pls investigate",
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
			}
		} else if (isNumberChanged.equals(CHANGE_DELETE)) {
			if (phoneNumberFromDB != null) {
				numberToSave = (EntityPhoneNumberInfo) SerializationUtils.clone(phoneNumberFromDB);

			} else {
				// TODO NINA
				System.out.println("Something is wrong this scenario should not happen - pls investigate");
				throw new VaadinUIException("Something is wrong this scenario should not happen - pls investigate",
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
			}
		}
		return numberToSave;
	}

	@Override
	public void beforeSaveValidations() {
		// Only validate phone number if it was changed or removed.
		// How do we validate if cell was removed...???

		// Validate Cell
		validateRequiredFields(contentViewDetail.getCellphoneNumber(),
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_CELL_INVALID);
		validateRequiredFields(contentViewDetail.getCellphoneCountryCode(),
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_CELL_DIAL_INVALID);

		validateIfFieldWasChanged(isCellNumberChanged, contentViewDetail.getCellphoneCountryCode(),
				contentViewDetail.getCellphoneNumber(),
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_CELL_DIAL_INVALID,
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_CELL_INVALID);

		// Validate Home
		validateRequiredFields(contentViewDetail.getHomePhoneNumber(),
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_HOME_INVALID);
		validateRequiredFields(contentViewDetail.getHomePhoneCountryCode(),
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_HOME_DIAL_INVALID);
		validateIfFieldWasChanged(isHomeNumberChanged, contentViewDetail.getHomePhoneCountryCode(),
				contentViewDetail.getHomePhoneNumber(),
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_HOME_DIAL_INVALID,
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_HOME_INVALID);

		// Validate Work
		validateRequiredFields(contentViewDetail.getWorkPhoneNumber(),
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_WORK_INVALID);
		validateRequiredFields(contentViewDetail.getWorkPhoneCountryCode(),
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_WORK_DIAL_INVALID);
		validateIfFieldWasChanged(isWorkNumberChanged, contentViewDetail.getWorkPhoneCountryCode(),
				contentViewDetail.getWorkPhoneNumber(),
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_WORK_DIAL_INVALID,
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_WORK_INVALID);

		// Validate Data
		validateRequiredFields(contentViewDetail.getDataTextField(),
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_DATA_INVALID);
		validateRequiredFields(contentViewDetail.getDataComboBox(),
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_DATA_DIAL_INVALID);
		validateIfFieldWasChanged(isDataNumberChanged, contentViewDetail.getDataComboBox(),
				contentViewDetail.getDataTextField(),
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_DATA_DIAL_INVALID,
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_DATA_INVALID);

		// Validate Account
		validateRequiredFields(contentViewDetail.getAccountPhoneNumber(),
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_ACCOUNT_INVALID);
		validateRequiredFields(contentViewDetail.getAccountPhoneCountryCode(),
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_ACCOUNT_DIAL_INVALID);
		validateIfFieldWasChanged(isStudAccNumberChanged, contentViewDetail.getAccountPhoneCountryCode(),
				contentViewDetail.getAccountPhoneNumber(),
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_ACCOUNT_DIAL_INVALID,
				ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_ACCOUNT_INVALID);

		validateRequiredFields(contentViewDetail.geteMailStudent(), "email is required");
		validateRequiredFields(contentViewDetail.geteMailStudentConfirm(), "email confirm is required");
		if (isEmailChanged != null) {
			if (!contentViewDetail.geteMailStudent().getValue()
					.equals(contentViewDetail.geteMailStudentConfirm().getValue())) {
				throw new VaadinUIException(ContactDetailsConstants.GeneralValidation.EMAIL_VALUE_MISMATCH,
						SeverityType.ERROR, Type.CLIENTSIDE, contentViewDetail.geteMailStudent());
			}
			validateEmailExternalWarningOnly(contentViewDetail.geteMailStudent());

		}

		
		validateRequiredFields(contentViewDetail.geteMailAccount(), "student account email is required");
		validateRequiredFields(contentViewDetail.geteMailAccountConfirm(), "student account email confirm is required");

		if (isStudentAccEmailChanged != null) {
			if (!contentViewDetail.geteMailAccount().getValue()
					.equals(contentViewDetail.geteMailAccountConfirm().getValue())) {
				throw new VaadinUIException(ContactDetailsConstants.GeneralValidation.EMAILACC_VALUE_MISMATCH,
						SeverityType.ERROR, Type.CLIENTSIDE, contentViewDetail.geteMailAccount());
			}
			validateEmailExternalWarningOnly(contentViewDetail.geteMailAccountConfirm());
		}
		resetNotificationPanel();
	}

	private void validateEmailExternalWarningOnly(TextField changedEmail) {
		try {
			log.info("applicationConfig.getAppName(): " + applicationConfig.getAppName());
			boolean isValid = contactService.validateEmailAddressExternal(
					changedEmail.getValue(), new ContextInfo(applicationConfig.getAppName()));

			// TODO Henriko - wat is die doel hiervan
			if (changedEmail.getValue() != null
					&& changedEmail.getValue().length() > 0) {
				for (char letter : changedEmail.getValue().toCharArray()) {

					if (Character.isUpperCase(letter)) {
						hasUppercase = true;
						break;
					}
				}
			}

		} catch (OperationFailedException e) {
			log.warn("Connection problem while calling ContactService on method validateEmailAddressExternal: "
					+ e.getMessage(), e);
			
//			setNotificationMsg(
//					"Connection problem while calling ContactService on method validateEmailAddressExternal: "
//							+ e.getMessage(),
//					SeverityType.WARNING);
//			throw new VaadinUIException("Connection problem while calling ContactService on method validateEmailAddressExternal: " + e.getMessage(),
//					SeverityType.ERROR, Type.CLIENTSIDE, changedEmail);
			throw new VaadinUIException(ContactDetailMessages.getString("emailerror.EmailMsg") +" " + e.getMessage(),
					SeverityType.ERROR, Type.CLIENTSIDE, changedEmail);

		} catch (DoesNotExistException | InvalidParameterException e) {
			log.warn("Invalid email while calling ContactService on method validateEmailAddressExternal: "
					+ e.getMessage(), e);
			setNotificationMsg("Invalid email while calling ContactService on method validateEmailAddressExternal: "
					+ e.getMessage(), SeverityType.WARNING);
			throw new VaadinUIException("Invalid email while calling ContactService on method validateEmailAddressExternal:",
					SeverityType.ERROR, Type.CLIENTSIDE, changedEmail);

		} catch (Exception ex) { // includes InvalidParameterException |
									// MissingParameterException |
									// PermissionDeniedException
			log.warn("Unforeseen exception occurred while calling ContactService on method validateEmailAddressExternal: "
							+ ex.getMessage(), ex);
//			setNotificationMsg("Connection problem while calling ContactService on method validateEmailAddressExternal: "
//							+ ex.getMessage(), SeverityType.WARNING);
			throw new VaadinUIException("Connection problem while calling ContactService on method validateEmailAddressExternal: ",
					SeverityType.ERROR, Type.CLIENTSIDE, changedEmail);
		}
	}

	private void validateRequiredFields(ComboBox comboBox, String errorMsg) {
		if (comboBox.isRequired()) {
			if (comboBox.getValue() == null || comboBox.isEmpty()) {
				throw new VaadinUIException(errorMsg, SeverityType.ERROR, Type.CLIENTSIDE, comboBox);

			}
		}
	}

	private void validateIfFieldWasChanged(String isNumberChanged, ComboBox countryCode, TextField phoneNumber,
			String errorDialCode, String errorPhoneNumber) {
		if (isNumberChanged != null) {
			if (isNumberChanged.equals(CHANGE_CREATE)) {
				validateDialAndPhone(countryCode, phoneNumber, true, errorDialCode, errorPhoneNumber);

			}
			if (isNumberChanged.equals(CHANGE_UPDATE)) {
				validateDialAndPhone(countryCode, phoneNumber, false, errorDialCode, errorPhoneNumber);

			}
		}
	}

	private void validateRequiredFields(TextField textField, String errorMsg) {
		if (textField.isRequired()) {
			if (textField.getValue() == null || textField.isEmpty()) {
				throw new VaadinUIException(errorMsg, SeverityType.ERROR, Type.CLIENTSIDE, textField);
			}
		}
	}

	public void validateDialAndPhone(ComboBox dialCode, TextField phoneNumber, Boolean create, String errorDialCode,
			String errorPhoneNumber) {
		if (!create) {
			if (!phoneNumber.isReadOnly()) {
				if (dialCode.getValue() == null) {
					// This is creating problems for me with null value or
					// something
					throw new VaadinUIException(errorDialCode, SeverityType.ERROR, Type.CLIENTSIDE, dialCode);
				}
				if (phoneNumber.getValue() == null || phoneNumber.getValue().length() == 0) {
					throw new VaadinUIException(errorPhoneNumber, SeverityType.ERROR, Type.CLIENTSIDE, phoneNumber);
				}
			}
		} else {
			if (!phoneNumber.isReadOnly()) {
				if (phoneNumber.getValue() == null || phoneNumber.getValue().length() == 0) {
					phoneNumber.setValue("");
				}
				if (phoneNumber.getValue() != null && dialCode.getValue() == null) {
					throw new VaadinUIException(errorDialCode, SeverityType.ERROR, Type.CLIENTSIDE, dialCode);

				} else if (phoneNumber.getValue() == null && dialCode.getValue() != null) {
					throw new VaadinUIException(errorPhoneNumber, SeverityType.ERROR, Type.CLIENTSIDE, phoneNumber);
				}
			}
		}
	}

	@Override
	public void loadInitialData() {
//		 lookupUser = "33711534";
		// lookupUser = "22931902";
//		 lookupUser = "31584675"; // Student only
		 // lookupUser = "11852267"; // Debtor only
		// lookupUser = "22144293"; // Employee only
		// lookupUser = "28866657"; // ?? 
//		 lookupUser = "28736257"; // ??
		log.info("Load initial data");
		setupBioGraphicInfo();
		setupCorrespondenceLang();
		setupAccToWhomTypeKeys();
		setupPostGradStudent();
		// Setup person Roles
		setupPersonRoles();
		validateAccessToContactDetailsApp();

		mapDTOtoUIComponents();
		setVisibilityOfPanelsBasedOnRoles();
		submitCancelView.getSubmitButton().setEnabled(true);

		setReadOnlyFields();
		setRequiredFieldsBasedOnRoles();
		//
		// TMC-166
		setCountryCodeRequiredIfPhoneNumberIsFilledIn(contentViewDetail.getCellphoneCountryCode(),
				contentViewDetail.getCellphoneNumber());
		setCountryCodeRequiredIfPhoneNumberIsFilledIn(contentViewDetail.getWorkPhoneCountryCode(),
				contentViewDetail.getWorkPhoneNumber());
		setCountryCodeRequiredIfPhoneNumberIsFilledIn(contentViewDetail.getHomePhoneCountryCode(),
				contentViewDetail.getHomePhoneNumber());
		setCountryCodeRequiredIfPhoneNumberIsFilledIn(contentViewDetail.getAccountPhoneCountryCode(),
				contentViewDetail.getAccountPhoneNumber());
		setCountryCodeRequiredIfPhoneNumberIsFilledIn(contentViewDetail.getDataComboBox(),
				contentViewDetail.getDataTextField());

		this.mapDTOtoUI = true;
		validateAccessToContactDetailsApp();
		showHideEmailConfirmField(false);
		showHideEmailAccountConfirmField(false);
		this.mapDTOtoUI = false;

	}

	private void setupCorrespondenceLang() {
		PersonPreferenceInfo result = personProxyService.getPersonPreferredCorrLanguage(lookupUser);
		if (result != null) {
			this.lookupUserCorLang = result.getPreferenceValue();
		} else {
			this.lookupUserCorLang = "vss.code.Language.3";		// Default language to English
		}
			
	}

	// Does a check to see who has access to use the app, currently only student
	// and employees
	private void validateAccessToContactDetailsApp() {
		// Univ input readonly unless Allow univ number input is configured true
		boolean allowaccess = false;

			if (!hasStudentRole && !hasEmployeeRole){
				allowaccess = false;
				disableExtenalaccess(allowaccess);
				setNotificationMsg(ContactDetailMessages.getString("error.on.access"), SeverityType.ERROR);
			}
			else if(hasExternalRole){
				allowaccess = false;
				disableExtenalaccess(allowaccess);
				setNotificationMsg(ContactDetailMessages.getString("error.on.access"), SeverityType.ERROR);
			}else{
				allowaccess = true;
				disableExtenalaccess(allowaccess);
			}
	}

	private void disableExtenalaccess(boolean externalAccessDisallowed) {
		this.contentViewDetail.getCellphoneNumber().setEnabled(externalAccessDisallowed);
		this.contentViewDetail.getAccountPhoneCountryCode().setEnabled(externalAccessDisallowed);
		this.contentViewDetail.geteMailAccount().setEnabled(externalAccessDisallowed);
		this.contentViewDetail.geteMailAccountConfirm().setEnabled(externalAccessDisallowed);
		this.contentViewDetail.geteMailStudent().setEnabled(externalAccessDisallowed);
		this.contentViewDetail.geteMailStudentConfirm().setEnabled(externalAccessDisallowed);
		this.contentViewDetail.getButtonRemoveCellPhoneNumber().setVisible(externalAccessDisallowed);
		this.submitCancelView.getSubmitButton().setEnabled(externalAccessDisallowed);
		this.contentViewDetail.getCellphoneCountryCode().setEnabled(externalAccessDisallowed);
		this.contentViewDetail.getDataComboBox().setEnabled(externalAccessDisallowed);
		this.contentViewDetail.getDataTextField().setEnabled(externalAccessDisallowed);
	}

	private void setRequiredFieldsBasedOnRoles() {

		contentViewDetail.getHomePhoneNumber().setRequired(false);
		contentViewDetail.geteMailAccount().setRequired(false);
		contentViewDetail.getAccountPhoneNumber().setRequired(false);
		contentViewDetail.getWorkPhoneNumber().setRequired(false);
		contentViewDetail.getDataTextField().setRequired(false);

		if (hasEmployeeRole) {
			contentViewDetail.getDataTextField().setRequiredError(
					ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_DATA_INVALID);
			// TMC-166 Work can't be required as employee can't change it
			// contentViewDetail.getWorkPhoneNumber().setRequired(true);
		}

		if (hasStudentRole) {
			contentViewDetail.getDataTextField().setRequiredError(
					ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_DATA_INVALID);
			contentViewDetail.getAccountPhoneNumber()
					.setRequiredError(ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_ACCOUNT_INVALID);
			contentViewDetail.getAccountPhoneNumber().setRequired(true);
			contentViewDetail.geteMailAccount().setRequired(true);
			this.contentViewDetail.getCellphoneNumber().setRequired(true);

		}

		if (isPostGradStudent) {
			contentViewDetail.getAccountPhoneNumber().setRequired(true);
			contentViewDetail.getAccountPhoneNumber()
					.setRequiredError(ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_ACCOUNT_INVALID);
			contentViewDetail.geteMailAccount().setRequired(true);
			this.contentViewDetail.getCellphoneNumber().setRequired(true);
			contentViewDetail.getWorkPhoneNumber().setRequired(true);

		}
	}

	private void setupBioGraphicInfo() {
		setSystemLanguage();
		personBiographicInfo = personProxyService.getPersonBiographicByLang(lookupUser, systemLanguageTypeKey, null);
	}

	private void setSystemLanguage() {
		String lang = UI.getCurrent().getLocale().getLanguage();
		if (lang.equals(LANGUAGE_AF)) {
			systemLanguageTypeKey = VaadinUIUtility.TYPE_KEY_SYSTEM_LANGUAGE_AF;

		} else {
			systemLanguageTypeKey = VaadinUIUtility.TYPE_KEY_SYSTEM_LANGUAGE_EN;
		}
	}

	private void setupAccToWhomTypeKeys() {

		setSystemLanguage();

		try {

			accToWhomTypeKeys = typeService.getTypesByCategory(ACCTOWHOM_TypeKey, systemLanguageTypeKey,
					this.readContextInfo);

		} catch (DoesNotExistException | InvalidParameterException | OperationFailedException
				| MissingParameterException | PermissionDeniedException e) {
			throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSTARTUPFAILURE, false);
		}
	}

	private void setupPostGradStudent() {
		isPostGradStudent = studentProxyService.isPostGradStudent(lookupUser, null);

	}

	/**
	 * @return
	 */
	private boolean hasWorkTelephoneRoles() {

		// Check if University number has Employee Rolet
		if (employeeRecordList != null && !employeeRecordList.isEmpty()) {
			return true;
		}
		// Check if University number has Post Graduation Role
		if (isPostGradStudent) {
			return true;
		}

		return false;
	}

	private void setVisibilityOfWorkPhoneFields(boolean show) {
		this.contentViewDetail.getWorkPhoneCountryCode()
				.setRequiredError(ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_WORK_DIAL_INVALID);
		this.contentViewDetail.getWorkPhoneCountryCode().setValidationVisible(show);

		this.contentViewDetail.getWorkPhoneNumber().setRequired(show);
		this.contentViewDetail.getWorkPhoneNumber()
				.setRequiredError(ContactDetailsConstants.GeneralValidation.PHONE_NUMBER_WORK_INVALID);
		this.contentViewDetail.getWorkPhoneNumber().setValidationVisible(show);

		if (show) {
			if (contentViewDetail.getFormLayout()
					.getComponentIndex(contentViewDetail.getHorizontalLayoutWorkNumber()) == -1) {
				contentViewDetail.getFormLayout().addComponent(contentViewDetail.getHorizontalLayoutWorkNumber(), 1);
			}
		} else {
			if (contentViewDetail.getFormLayout()
					.getComponentIndex(contentViewDetail.getHorizontalLayoutWorkNumber()) != -1) {
				contentViewDetail.getFormLayout().removeComponent(contentViewDetail.getHorizontalLayoutWorkNumber());
			}
		}
	}

	@Override
	public void refreshData() {
		isCellNumberChanged = null;
		isHomeNumberChanged = null;
		isStudAccNumberChanged = null;
		isWorkNumberChanged = null;
		isDataNumberChanged = null;
		cellPhoneNumber = null;
		dataPhoneNumber = null;
		homePhoneNumber = null;
		studentAccountPhoneNumber = null;
		workPhoneNumber = null;

		resetUI();
		loadInitialData();
	}

	@Override
	public void populateUIComboComponents() {
		countryPhoneCodes = countryInformationService.getAllCountryPhoneCodes();
		countryPhoneCodesDescription = addCountryNameToDropList(countryPhoneCodes);

		this.contentViewDetail.getCellphoneCountryCode().addItems(countryPhoneCodesDescription.keySet());
		this.contentViewDetail.getWorkPhoneCountryCode().addItems(countryPhoneCodesDescription.keySet());
		this.contentViewDetail.getHomePhoneCountryCode().addItems(countryPhoneCodesDescription.keySet());
		this.contentViewDetail.getAccountPhoneCountryCode().addItems(countryPhoneCodesDescription.keySet());
		this.contentViewDetail.getDataComboBox().addItems(countryPhoneCodesDescription.keySet());

		for (String phoneCodes : countryPhoneCodesDescription.keySet()) {
			this.contentViewDetail.getCellphoneCountryCode().setItemIcon(phoneCodes,
					new ClassResource(countryPhoneCodesDescription.get(phoneCodes)));
			this.contentViewDetail.getWorkPhoneCountryCode().setItemIcon(phoneCodes,
					new ClassResource(countryPhoneCodesDescription.get(phoneCodes)));
			this.contentViewDetail.getHomePhoneCountryCode().setItemIcon(phoneCodes,
					new ClassResource(countryPhoneCodesDescription.get(phoneCodes)));
			this.contentViewDetail.getAccountPhoneCountryCode().setItemIcon(phoneCodes,
					new ClassResource(countryPhoneCodesDescription.get(phoneCodes)));
			this.contentViewDetail.getDataComboBox().setItemIcon(phoneCodes,
					new ClassResource(countryPhoneCodesDescription.get(phoneCodes)));
		}
	}

	public Map<String, String> addCountryNameToDropList(Map<String, String> countryCodes) {
		Map<String, String> countryCodesWithDescription = new TreeMap<>();
		for (Map.Entry<String, String> countryAndPhoneCode : countryCodes.entrySet()) {
			String key = countryAndPhoneCode.getKey().trim();
			String value = countryAndPhoneCode.getValue();

			if (value.equals("http://www.oorsprong.org/WebSamples.CountryInfo/Images/Saudia_Arabiajpg")) {
				value = "http://www.oorsprong.org/WebSamples.CountryInfo/Images/Saudia_Arabia.jpg";
			}
			String country = value.substring(value.lastIndexOf('/') + 1).replace('_', ' ');
			countryCodesWithDescription.put(country.substring(0, country.lastIndexOf('.')) + " - " + key, value);
		}
		return countryCodesWithDescription;
	}

	public void setInitialDialCode(ComboBox dialCodeComponent, String value) {
		// Convert countryPhoneCodes to countryPhoneCodesDescription Value
		if (value != null) {
			log.info("setInitialDialCode: " + countryPhoneCodesDescription.size() + ". \n" + "Key: " + value);
			for (Map.Entry<String, String> e : countryPhoneCodesDescription.entrySet()) {
				if (e.getKey().contains(" " + value + " ")) {
					value = e.getKey();
					break;
				}
			}

			// Only set dial code if it exist in the list. Otherwise user must
			// select new dial code (No default to 27 (ZA))
			if (countryPhoneCodesDescription.containsKey(value)) {
				dialCodeComponent.select(value);
			}
		}
	}

	@Override
	public void resetUI() {
		try {

			uiReset = true;
			mapDTOtoUI = true;
			resetNotificationPanel();
			this.contentViewDetail.getOutputUniversityNumber().setComponentError(null);
			this.contentViewDetail.getOutputUniversityNumber().setValidationVisible(false);

			this.contentViewDetail.getOutputUniversityNumberLabel().setVisible(false);
			this.contentViewDetail.getOutputUniversityNumberLabel().setValue(null);

			this.contentViewDetail.getOutputTitleInitialsSurname().setValue(null);

			this.contentViewDetail.getCellphoneCountryCode().setReadOnly(false);
			this.contentViewDetail.getCellphoneCountryCode().setValue(null);
			this.contentViewDetail.getCellphoneCountryCode().setComponentError(null);
			this.contentViewDetail.getCellphoneCountryCode().setValidationVisible(false);

			this.contentViewDetail.getCellphoneNumber().setNullRepresentation("");
			this.contentViewDetail.getCellphoneNumber().setReadOnly(false);
			// this.contentViewDetail.getCellphoneNumber().setValue(null);
			this.contentViewDetail.getCellphoneNumber().setRequired(false);
			this.contentViewDetail.getCellphoneNumber().setComponentError(null);
			this.contentViewDetail.getCellphoneNumber().setValidationVisible(false);

			this.contentViewDetail.getWorkPhoneCountryCode().setReadOnly(false);
			this.contentViewDetail.getWorkPhoneCountryCode().setValue(null);
			this.contentViewDetail.getWorkPhoneCountryCode().setComponentError(null);
			this.contentViewDetail.getWorkPhoneCountryCode().setValidationVisible(false);

			this.contentViewDetail.getWorkPhoneNumber().setNullRepresentation("");
			this.contentViewDetail.getWorkPhoneNumber().setReadOnly(false);
			// this.contentViewDetail.getWorkPhoneNumber().setValue(null);
			this.contentViewDetail.getWorkPhoneNumber().setRequired(false);
			this.contentViewDetail.getWorkPhoneNumber().setComponentError(null);
			this.contentViewDetail.getWorkPhoneNumber().setValidationVisible(false);

			this.contentViewDetail.getHomePhoneCountryCode().setReadOnly(false);
			this.contentViewDetail.getHomePhoneCountryCode().setValue(null);
			this.contentViewDetail.getHomePhoneCountryCode().setComponentError(null);
			this.contentViewDetail.getHomePhoneCountryCode().setValidationVisible(false);

			this.contentViewDetail.getHomePhoneNumber().setNullRepresentation("");
			this.contentViewDetail.getHomePhoneNumber().setReadOnly(false);
			// this.contentViewDetail.getHomePhoneNumber().setValue(null);
			this.contentViewDetail.getHomePhoneNumber().setRequired(false);
			this.contentViewDetail.getHomePhoneNumber().setComponentError(null);
			this.contentViewDetail.getHomePhoneNumber().setValidationVisible(false);

			this.contentViewDetail.getAccountPhoneCountryCode().setReadOnly(false);
			this.contentViewDetail.getAccountPhoneCountryCode().setValue(null);

			this.contentViewDetail.getAccountPhoneNumber().setNullRepresentation("");
			this.contentViewDetail.getAccountPhoneNumber().setReadOnly(false);
			// this.contentViewDetail.getAccountPhoneNumber().setValue(null);
			this.contentViewDetail.getAccountPhoneNumber().setRequired(false);

			this.contentViewDetail.getDataNumberLayout().setVisible(false);
			this.contentViewDetail.getDataComboBox().setRequired(false);
			this.contentViewDetail.getDataTextField().setRequired(false);

			this.contentViewDetail.geteMailStudent().setReadOnly(false);
			// this.contentViewDetail.geteMailStudent().setValue(null);
			this.contentViewDetail.geteMailStudent().setNullRepresentation("");
			this.contentViewDetail.geteMailStudent().setRequired(false);
			this.contentViewDetail.geteMailStudent().setComponentError(null);
			this.contentViewDetail.geteMailStudent().setValidationVisible(false);

			// this.contentViewDetail.geteMailStudentConfirm().setValue(null);
			this.contentViewDetail.geteMailStudentConfirm().setNullRepresentation("");
			this.contentViewDetail.geteMailStudentConfirm().setRequired(false);
			this.contentViewDetail.geteMailStudentConfirm().setComponentError(null);
			this.contentViewDetail.geteMailStudentConfirm().setValidationVisible(false);

			// this.contentViewDetail.geteMailAccount().setValue(null);
			this.contentViewDetail.geteMailAccount().setNullRepresentation("");
			this.contentViewDetail.geteMailAccount().setRequired(false);
			this.contentViewDetail.geteMailAccount().setComponentError(null);
			this.contentViewDetail.geteMailAccount().setValidationVisible(false);

			// this.contentViewDetail.geteMailAccountConfirm().setValue(null);
			this.contentViewDetail.geteMailAccountConfirm().setNullRepresentation("");
			this.contentViewDetail.geteMailAccountConfirm().setRequired(false);
			this.contentViewDetail.geteMailAccountConfirm().setComponentError(null);
			this.contentViewDetail.geteMailAccountConfirm().setValidationVisible(false);

			this.contentViewDetail.getButtonRemoveCellPhoneNumber().setVisible(true);

			doSave = true;
			isCellNumberChanged = null;
			isEmailChanged = null;
			isHomeNumberChanged = null;
			isStudAccNumberChanged = null;
			isStudentAccEmailChanged = null;
			isDataNumberChanged = null;
			isWorkNumberChanged = null;
			
			uiReset = false;
			mapDTOtoUI = false;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void afterSave() {
		if (doSave) {
			// TODO Henriko - Wat maak dit saak of die email uppercase chars in het???
			if (hasUppercase) {
				throw new VaadinUIException("Email is in the incorrect format. Saving regardless.",
						SeverityType.WARNING, Type.CLIENTSIDE);
			}
		} else {
			doSave = true;
			throw new VaadinUIException(SAVE_UNSUCCESSFUL, SeverityType.ERROR, Type.SAVE);
		}
	}

	@Override
	public void beforeSave() {
		mapUIComponentsToDTO();
	}

	@Override
	public void save() {
		try {

			if (doSave) {

				if(isCellNumberChanged != null || isEmailChanged != null) {
					
					String newEmailAddress = null;
					if (emailVirtualAddressInfoToSave != null) {
						newEmailAddress = emailVirtualAddressInfoToSave.getVirtualAddress();						
					} 
					otpDialog = new OTPDialog(cellPhoneNumberToSave, emailVirtualAddressInfoToSave, 
							appPropertyFileName, lookupUser, authenticatedUser, 
							isCellNumberChanged, isEmailChanged, this, 
							contentViewDetail, this.lookupUserCorLang, newEmailAddress );
					otpDialog.center();
					otpDialog.setStyleName("v-dialog-caption-dialogheader");
					otpDialog.setCaption("OTP Dialog");
					otpDialog.setResizable(false);
					otpDialog.setWidth("500");
					otpDialog.setHeight("300");

					UI.getCurrent().addWindow(otpDialog);
				} else {
					continueSave();
				}

				// TMC-88 Save the preferences
				// PersonProxyServiceCRUD personProxyServiceCRUD = new
				// PersonProxyServiceCRUD(applicationConfig);
				//
				// PersonPreferenceInfo personPreferenceInfo = new
				// PersonPreferenceInfo();
				// personPreferenceInfo.setPreferenceTypeKey(ACCTOWHOM_TypeKey.concat(".").concat(contentViewDetail.getAccountToCombo().getValue().toString()));
				// personPreferenceInfo.setPreferenceValue(contentViewDetail.getAccountAddresseeText().getValue());
				// personPreferenceInfo.setUnivNumber(lookupUser);

				// TMC-88 Not making call until backend are ready.
				// PersonPreferenceInfo response =
				// personProxyServiceCRUD.createPersonPreference(personPreferenceInfo,
				// null);
			}
		} catch (VaadinUIException e) {
			log.error("Could not save: " + e.getMessage(), e);
			checkForHTTPException(e);
			System.out.println(e.getMessage());
			throw new VaadinUIException(MSG_UNKNOWN + e.getMessage(), SeverityType.ERROR, Type.SERVERSIDE);

		} catch (Exception e) {
			log.error("Could not save: " + e.getMessage(), e);
			checkForHTTPException(e);
			System.out.println(e.getMessage());
			throw new VaadinUIException(MSG_UNKNOWN + e.getMessage(), SeverityType.ERROR, Type.SERVERSIDE);

		}

	}

	public void continueSave(){
		try {
			saveCellPhoneNumber();
			saveWorkPhoneNumber();
			saveHomePhoneNumber();
            saveDataPhoneNumber();
            saveStudentAccountPhoneNumber();
			saveEmail();
			saveStudentAccountEmail();
//			refreshData();
			Collection<Window> windows = UI.getCurrent().getWindows();
			if(windows != null){
				for(Window window : windows){
					UI.getCurrent().removeWindow(window);
				}
			}
			Notification.show("Save Successful");
		} catch (Exception e) {
			throw new VaadinUIException(MSG_UNKNOWN + e.getMessage(), SeverityType.ERROR, Type.SERVERSIDE);
		}
	}

	private void saveStudentAccountEmail() {
		try {
			if (isStudentAccEmailChanged != null) {
				contactServiceCRUD.maintainEntityVirtualAddressWithoutExternalVerification(
						accEmailVirtualAddressInfoToSave, this.readContextInfo);
				
			}
		} catch (DoesNotExistException | InvalidParameterException | OperationFailedException
				| MissingParameterException | PermissionDeniedException | VersionMismatchException e) {
			throw new VaadinUIException(e.getMessage(), SeverityType.ERROR, Type.SAVE,
					contentViewDetail.geteMailAccount());
		}
	}

	private void saveEmail() {
		try {
			if (isEmailChanged != null) {
				contactServiceCRUD.maintainEntityVirtualAddressWithoutExternalVerification(emailVirtualAddressInfoToSave,
						this.readContextInfo);
			}
		} catch (DoesNotExistException | InvalidParameterException | OperationFailedException
				| MissingParameterException | PermissionDeniedException | VersionMismatchException e) {
			throw new VaadinUIException(e.getMessage(), SeverityType.ERROR, Type.SAVE,
					contentViewDetail.geteMailStudent());
		}
	}

	private void saveStudentAccountPhoneNumber() {
		if (isStudAccNumberChanged != null) {
			try {
				if (isStudAccNumberChanged.equals(CHANGE_CREATE)) {
					writeObjectBeforeSave(studentAccountPhoneNumberToSave, "saveStudentAccountPhoneNumber : Insert");
					contactServiceCRUD.insertEntityPhoneNumber(studentAccountPhoneNumberToSave, this.readContextInfo);

				}
				if (isStudAccNumberChanged.equals(CHANGE_UPDATE)) {
					writeObjectBeforeSave(studentAccountPhoneNumberToSave, "saveStudentAccountPhoneNumber : Update");
					contactServiceCRUD.updateEntityPhoneNumber(studentAccountPhoneNumberToSave, this.readContextInfo);

				}

			} catch (DoesNotExistException | InvalidParameterException | OperationFailedException
					| MissingParameterException | PermissionDeniedException | VersionMismatchException e) {
				throw new VaadinUIException(e.getMessage(), SeverityType.ERROR, Type.SAVE,
						contentViewDetail.getAccountPhoneNumber());
			}

		}

	}

	private void saveDataPhoneNumber() {
		if (isDataNumberChanged != null) {
			try {
				if (isDataNumberChanged.equals(CHANGE_CREATE)) {
					writeObjectBeforeSave(dataPhoneNumberToSave, "saveDataPhoneNumber : Insert");
					contactServiceCRUD.insertEntityPhoneNumber(dataPhoneNumberToSave, this.readContextInfo);

				}
				if (isDataNumberChanged.equals(CHANGE_UPDATE)) {
					writeObjectBeforeSave(dataPhoneNumberToSave, "saveDataPhoneNumber : Update");
					contactServiceCRUD.updateEntityPhoneNumber(dataPhoneNumberToSave, this.readContextInfo);
				}
				if (isDataNumberChanged.equals(CHANGE_DELETE)) {
					writeObjectBeforeSave(homePhoneNumberToSave,
							"saveHomePhoneNumber : deletePhoneNumberByPurposeType");
					contactServiceCRUD.deletePhoneNumberByPurposeType(lookupUser,
							dataPhoneNumberToSave.getPhoneNumberPurposeTypeKey(), this.readContextInfo);

				}

			} catch (DoesNotExistException | InvalidParameterException | OperationFailedException
					| MissingParameterException | PermissionDeniedException | VersionMismatchException e) {
				throw new VaadinUIException(e.getMessage(), SeverityType.ERROR, Type.SAVE,
						contentViewDetail.getDataTextField());
			}

		}

	}

	private void saveHomePhoneNumber() {
		if (isHomeNumberChanged != null) {
			try {
				if (isHomeNumberChanged.equals(CHANGE_CREATE)) {
					writeObjectBeforeSave(homePhoneNumberToSave, "saveHomePhoneNumber : insertEntityPhoneNumber");
					contactServiceCRUD.insertEntityPhoneNumber(homePhoneNumberToSave, this.readContextInfo);

				}
				if (isHomeNumberChanged.equals(CHANGE_UPDATE)) {
					writeObjectBeforeSave(homePhoneNumberToSave, "saveHomePhoneNumber : updateEntityPhoneNumber");
					contactServiceCRUD.updateEntityPhoneNumber(homePhoneNumberToSave, this.readContextInfo);

				}
				if (isHomeNumberChanged.equals(CHANGE_DELETE)) {
					writeObjectBeforeSave(homePhoneNumberToSave,
							"saveHomePhoneNumber : deletePhoneNumberByPurposeType");
					contactServiceCRUD.deletePhoneNumberByPurposeType(lookupUser,
							homePhoneNumberToSave.getPhoneNumberPurposeTypeKey(), this.readContextInfo);

				}

			} catch (DoesNotExistException | InvalidParameterException | OperationFailedException
					| MissingParameterException | PermissionDeniedException | VersionMismatchException e) {
				log.error("Could not preform action on record: " + e.getMessage(), e);
				throw new VaadinUIException(e.getMessage(), SeverityType.ERROR, Type.SAVE,
						contentViewDetail.getHomePhoneNumber());
			}

		}
	}

	private void saveWorkPhoneNumber() {
		if (isWorkNumberChanged != null) {
			try {
				if (isWorkNumberChanged.equals(CHANGE_CREATE)) {
					writeObjectBeforeSave(workPhoneNumberToSave, "saveWorkPhoneNumber : insertEntityPhoneNumber");
					contactServiceCRUD.insertEntityPhoneNumber(workPhoneNumberToSave, this.readContextInfo);
				}
				if (isWorkNumberChanged.equals(CHANGE_UPDATE)) {
					writeObjectBeforeSave(workPhoneNumberToSave, "saveWorkPhoneNumber : updateEntityPhoneNumber");
					contactServiceCRUD.updateEntityPhoneNumber(workPhoneNumberToSave, this.readContextInfo);

				}

			} catch (DoesNotExistException | InvalidParameterException | OperationFailedException
					| MissingParameterException | PermissionDeniedException | VersionMismatchException e) {
				throw new VaadinUIException(e.getMessage(), SeverityType.ERROR, Type.SAVE,
						contentViewDetail.getWorkPhoneNumber());
			}
		}
	}

	private void saveCellPhoneNumber() {
		try {
			if (isCellNumberChanged != null) {
				if (isCellNumberChanged.equals(CHANGE_CREATE)) {
					writeObjectBeforeSave(cellPhoneNumberToSave, "saveCellPhone:insertEntityPhoneNumber");
					contactServiceCRUD.insertEntityPhoneNumber(cellPhoneNumberToSave, this.readContextInfo);
				}
				if (isCellNumberChanged.equals(CHANGE_UPDATE)) {
					writeObjectBeforeSave(cellPhoneNumberToSave, "saveCellPhone:updateEntityPhoneNumber");
					try {
						contactServiceCRUD.updateEntityPhoneNumber(cellPhoneNumberToSave, this.readContextInfo);
					}catch (DoesNotExistException   | OperationFailedException | InvalidParameterException ex){
						log.error(ContactDetailMessages.getString("emailerror.EmailMsg") + ex.getMessage());
						throw new VaadinUIException(ContactDetailMessages.getString("emailerror.EmailMsg") ,
								SeverityType.ERROR, Type.CLIENTSIDE, contentViewDetail.getCellphoneNumber());

					}
				}
				if (isCellNumberChanged.equals(CHANGE_DELETE)) {
					writeObjectBeforeSave(cellPhoneNumberToSave, "saveCellPhone:deletePhoneNumberByPurposeType");
					contactServiceCRUD.deletePhoneNumberByPurposeType(lookupUser,
							cellPhoneNumberToSave.getPhoneNumberPurposeTypeKey(), this.readContextInfo);
				}
			}
		}
		catch(OperationFailedException operationFailedException){
			throw new VaadinUIException(ContactDetailMessages.getString("emailerror.EmailMsg") +" " + operationFailedException.getMessage(),

					SeverityType.ERROR, Type.CLIENTSIDE, contentViewDetail.getCellphoneNumber());
		}catch (DoesNotExistException|  InvalidParameterException
				| MissingParameterException | PermissionDeniedException | VersionMismatchException e) {
			log.error("Could not insert/update/delete cell phone number: " + e.getMessage(), e);
			throw new VaadinUIException(e.getMessage(), SeverityType.ERROR, Type.SAVE,
					contentViewDetail.getCellphoneNumber());

		}
	}

	private void writeObjectBeforeSave(Object objToWrite, String logMessage) {
		if (objToWrite != null) {
			try {
				log.info(logMessage + getObjectWriter().writeValueAsString(objToWrite));
			} catch (IOException e) {
				log.warn("Could not writeObjectBeforeSave to string for class : " + objToWrite.getClass().getName()
						+ " " + e.getMessage(), e);
			}
		}
	}

	private ObjectWriter getObjectWriter() {
		if (objWriter == null) {
			objWriter = new ObjectMapper().writerWithDefaultPrettyPrinter();
		}
		return objWriter;

	}

	public String getShortDialCode(ComboBox dialCodeComboBox) {
		if (dialCodeComboBox.getValue() != null) {
			String longDialCode = dialCodeComboBox.getValue().toString();
			return longDialCode.substring(longDialCode.indexOf("- ") + 2, longDialCode.lastIndexOf(" ("));
		} else {
			return null;
		}
	}

	private EntityVirtualAddressInfo setupEmailToSave(String uiEmail, String isEmailChanged, EntityVirtualAddressInfo emailFromDB, String emailTypeKey) {
		EntityVirtualAddressInfo emailToSave = null;
		if (isEmailChanged.equals(CHANGE_CREATE)) {
			// Construct a new object
			emailToSave = new EntityVirtualAddressInfo();
			MetaInfo metaInfo = new MetaInfo();
			metaInfo.setUpdateTime(new Date());
			metaInfo.setUpdateId(authenticatedUser);
			metaInfo.setAuditFunction(authenticatedUser);
			metaInfo.setCreateId(authenticatedUser);
			emailToSave.setMetaInfo(metaInfo);
			emailToSave.setUnivNumber(lookupUser);
			emailToSave.setVirtualAddressPurposeTypeKey(emailTypeKey);
			emailToSave.setVirtualAddressCategoryTypeKey(ContactDetailsConstants.CONTACT_SERVICE_VIRTUALADDRESS_CATEGORY);
			emailToSave.setVirtualAddress(uiEmail);
			emailToSave.setStartDate(new Date());
		} else if (isEmailChanged.equals(CHANGE_UPDATE)) {
			if (emailFromDB != null) {
				// Make a copy of the object that was read from DB before any
				// changes
				emailToSave = (EntityVirtualAddressInfo) SerializationUtils.clone(emailFromDB);
//				MetaInfo metaInfo = emailToSave.getMetaInfo();
//				metaInfo.setVersionInd(metaInfo.getVersionInd());
				emailToSave.getMetaInfo().setUpdateTime(new Date());
//				metaInfo.setAuditFunction(metaInfo.getAuditFunction());
				emailToSave.getMetaInfo().setUpdateId(authenticatedUser);
				emailToSave.setVirtualAddress(uiEmail);
				emailToSave.setStartDate(new Date());

				emailToSave.setVirtualAddress(uiEmail);

			} else {
				// TODO NINA
				System.out.println("Something is wrong this scenario should not happen - pls investigate");
				throw new VaadinUIException("Something is wrong this scenario should not happen - pls investigate",
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
			}
		} 
		return emailToSave;
	}

	public EntityPhoneNumberInfo getPersonDialCodePhoneNumberInfo(String phoneNumberType, String dialCode,
			String phoneNumber, Boolean create) {
		EntityPhoneNumberInfo personPhoneNumber = null;
		if (!create) {
			try {
				personPhoneNumber = contactService.getEntityPhoneNumberByPurposeType(lookupUser, phoneNumberType,
						this.readContextInfo);
			} catch (DoesNotExistException | InvalidParameterException | MissingParameterException
					| PermissionDeniedException | OperationFailedException e) {
				throw new VaadinUIException(e, SeverityType.ERROR, null, Type.CLIENTSIDE, false);
			}

			if (personPhoneNumber != null) {
				MetaInfo metaInfo = personPhoneNumber.getMetaInfo();
				metaInfo.setVersionInd(metaInfo.getVersionInd());
				metaInfo.setUpdateTime(new Date());
				metaInfo.setAuditFunction(metaInfo.getAuditFunction());
				metaInfo.setUpdateId(authenticatedUser);
				personPhoneNumber.setUnivNumber(lookupUser);
				personPhoneNumber.phoneNumberPurposeTypeKey = phoneNumberType;
				personPhoneNumber.setPhoneNumber(phoneNumber);
				personPhoneNumber.setCountryDailPrefix(dialCode);
				personPhoneNumber.setStartDate(new Date());
			}
		} else {
			personPhoneNumber = new EntityPhoneNumberInfo();
			MetaInfo metaInfo = new MetaInfo();
			metaInfo.setUpdateTime(new Date());
			metaInfo.setUpdateId(authenticatedUser);
			metaInfo.setAuditFunction(authenticatedUser);
			metaInfo.setCreateId(authenticatedUser);
			personPhoneNumber.setMetaInfo(metaInfo);
			personPhoneNumber.setUnivNumber(lookupUser);
			personPhoneNumber.phoneNumberPurposeTypeKey = phoneNumberType;
			personPhoneNumber.setPhoneNumber(phoneNumber);
			personPhoneNumber.setCountryDailPrefix(dialCode);
			personPhoneNumber.setStartDate(new Date());
		}

		return personPhoneNumber;
	}

	@Override
	public void setAppPropertyFileName() {
		this.appPropertyFileName = CONFIG_PROPERTIES_FILENAME;

	}

	@Override
	public void readApplicationPropertyFile() {

		try {
			wsHRUser = propertyValueRetriever.getPropertyValues(ContactDetailsConstants.USER_NAME,
					ContactDetailsConstants.PROPERTY_FILE_NAME);
			wsHRPassword = propertyValueRetriever.getPropertyValues(ContactDetailsConstants.PASSWORD,
					ContactDetailsConstants.PROPERTY_FILE_NAME);
		} catch (IOException e1) {
			throw new VaadinUIException(e1, SeverityType.ERROR, null, Type.PROPERTYFILE, false);
		}

	}

	public void checkForHTTPException(Throwable throwable) {
		String msg = throwable.getMessage();
		boolean result = msg.contains(ERROR_HTTP_RESPONSE_401_UNAUTHORIZED_WHEN_COMMUNICATING);
		if (result) {
			throw new VaadinUIException(throwable.getMessage(), SeverityType.ERROR, Type.SERVERSIDE);
		} else {
			Throwable level1 = throwable.getCause();
			msg = level1.getMessage();
			result = msg.contains(ERROR_HTTP_RESPONSE_401_UNAUTHORIZED_WHEN_COMMUNICATING);
			if (result) {
				throw new VaadinUIException(level1.getMessage(), SeverityType.ERROR, Type.SERVERSIDE);
			} else {
				Throwable level2 = throwable.getCause();
				msg = level2.getMessage();
				result = msg.contains(ERROR_HTTP_RESPONSE_401_UNAUTHORIZED_WHEN_COMMUNICATING);
				if (result) {
					throw new VaadinUIException(level2.getMessage(), SeverityType.ERROR, Type.SERVERSIDE);
				} else {
					System.out.println("doh");
				}
			}
		}
	}

	// If phone number is filled out but not the phone dial code then the phone
	// dial code becomes required
	private void setCountryCodeRequiredIfPhoneNumberIsFilledIn(ComboBox comboBox, TextField phoneNumber) {

			if (phoneNumber != null && !phoneNumber.isEmpty()) {
				if (!comboBox.isReadOnly()) { // TMC-166 Can't make combo
												// required if it is read only
					comboBox.setRequired(true);						
				}
			} else {
				comboBox.setRequired(false);
			}
		
	}

	private String determineCountryDialCodeUpdateOrCreate(String changedcountryCode,
			EntityPhoneNumberInfo phoneNumber) {
		String changeStatus = null;
		if (phoneNumber == null) { // Not on DB
			if (!changedcountryCode.isEmpty()) {
				changeStatus = CHANGE_CREATE;
			}
		} else { // On db but empty
			if (phoneNumber.getCountryDailPrefix() == null) {
				if (!changedcountryCode.isEmpty()) {
					changeStatus = CHANGE_UPDATE;
				} else {
					// Ignore
				}

			} else {
				if (!phoneNumber.getCountryDailPrefix().equals(changedcountryCode)) {
					changeStatus = CHANGE_UPDATE;
				} else {
					// Ignore
				}
			}
		}
//		if (phoneNumber != null) {
//			System.out.println(phoneNumber.getCountryDailPrefix() + " vs " + changedcountryCode + " changeStatus = : "
//					+ changeStatus);
//
//		} else {
//			System.out.println("null " + " vs " + changedcountryCode + " changeStatus = : " + changeStatus);
//
//		}
		return changeStatus;
	}

	public String determinePhoneNumberUpdateOrCreate(String changedPhoneNumber, EntityPhoneNumberInfo phoneNumber) {
		String changeStatus = null;
		if (phoneNumber == null) { // Not on DB
			if (!changedPhoneNumber.isEmpty()) {
				changeStatus = CHANGE_CREATE;
			}
		} else { // On db but empty
			if (phoneNumber.getPhoneNumber() == null) {
				if (!changedPhoneNumber.isEmpty()) {
					changeStatus = CHANGE_UPDATE;
				} else {
					// Ignore
				}

			} else {
				if (!phoneNumber.getPhoneNumber().equals(changedPhoneNumber)) {
					changeStatus = CHANGE_UPDATE;
				} else {
					// Ignore
				}
			}
		}
//		if (phoneNumber != null) {
//			System.out.println(
//					phoneNumber.getPhoneNumber() + " vs " + changedPhoneNumber + " changeStatus = : " + changeStatus);
//
//		} else {
//			System.out.println("null" + " vs " + changedPhoneNumber + " changeStatus = : " + changeStatus);
//
//		}
		return changeStatus;
	}

	// Reset all errors on the ComboBox
	private void resetValidationMessagesOnComboBox(ComboBox uiComboBox) {
		uiComboBox.setComponentError(null);
		uiComboBox.setValidationVisible(false);
	}

	// Reset all errors on the TextField
	private void resetValidationMessageOnTextField(TextField textFieldComponent) {
		textFieldComponent.setComponentError(null);
		textFieldComponent.setValidationVisible(false);
	}

	public String determineEmailUpdateOrCreate(String changedEmail, EntityVirtualAddressInfo email) {
		String changeStatus = null;
		if (email == null) { // Not on DB
			if (!changedEmail.isEmpty()) {
				changeStatus = CHANGE_CREATE;
			}
		} else { // On db but empty
			if (email.getVirtualAddress() == null) {
				if (!changedEmail.isEmpty()) {
					changeStatus = CHANGE_UPDATE;
				} else {
					// Ignore
				}

			} else {
				if (!email.getVirtualAddress().equals(changedEmail)) {
					changeStatus = CHANGE_UPDATE;
				} else {
					// Ignore
				}
			}
		}
//		if (email != null) {
//			System.out.println(email.getVirtualAddress() + " vs " + changedEmail + " changeStatus = : " + changeStatus);
//
//		} else {
//			System.out.println("null" + " vs " + changedEmail + " changeStatus = : " + changeStatus);
//		}
		return changeStatus;
	}
}
