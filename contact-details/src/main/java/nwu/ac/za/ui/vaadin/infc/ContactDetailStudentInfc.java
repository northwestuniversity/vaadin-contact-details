package nwu.ac.za.ui.vaadin.infc;

import com.vaadin.ui.*;

public interface ContactDetailStudentInfc {

	VerticalLayout getVerticalLayout();

	TextField getOutputUniversityNumber();

	Label getOutputUniversityNumberLabel();

	Label getOutputTitleInitialsSurname();

	ComboBox getCellphoneCountryCode();

	TextField getCellphoneNumber();

	HorizontalLayout getHorizontalLayoutWorkNumber();

	ComboBox getWorkPhoneCountryCode();

	TextField getWorkPhoneNumber();

	ComboBox getHomePhoneCountryCode();

	TextField getHomePhoneNumber();

	HorizontalLayout getHorizontalLayoutAccountNumber();

	Label getAccountNumber();

	ComboBox getAccountPhoneCountryCode();

	TextField getAccountPhoneNumber();

	TextField geteMailStudent();

	TextField geteMailStudentConfirm();

	TextField geteMailAccount();

	TextField geteMailAccountConfirm();

	Label getUnivNumber();

	Label getCellNumber();

	Label getWorkNumber();

	Label getHomeNumber();

	Label geteMailAddress();

	Label getConfirmEMailAddress();

	HorizontalLayout getHorizontalLayoutConfirmEmail();

	Label getAccountEMail();

	Label getConfirmAccountEMail();

	HorizontalLayout getHorizontalLayoutConfirmAccountEmail();

	VerticalLayout getStudentEmailLayout();

	FormLayout getFormLayout();

	Button getButtonRemoveHomePhoneNumber();
	
	Button getButtonRemoveCellPhoneNumber();

	 Label getAccountTo();

	 ComboBox getAccountToCombo();

	 Label getAccountAddressee();

    TextField getAccountAddresseeText();

	HorizontalLayout getHorizontalAccountTo();

	HorizontalLayout getHorizontalAddressee();

	HorizontalLayout getHomeNumberLayout();

	HorizontalLayout getDataNumberLayout();

	Label getDataNumberLabel();

	Button getDataClearButton();

	TextField getDataTextField();

	ComboBox getDataComboBox();

	HorizontalLayout getAddressForAccountLayout();
}
