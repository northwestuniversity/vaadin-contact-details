package nwu.ac.za.service;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import ac.za.nwu.notification.dto.notifications.NotificationInfo;
import ac.za.nwu.notification.service.NotificationCompService;
import ac.za.nwu.notification.service.NotificationCompServiceNamespace;
import ac.za.nwu.registry.utility.GenericServiceClientFactory;
import ac.za.nwu.utility.EncryptorUtility;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import nwu.ac.za.config.ApplicationConfig;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.ac.za.util.ContactDetailMessages;
import nwu.ac.za.util.ContactDetailsConstants;


public class NotificationCompProxyService extends AbstractServiceProxy<NotificationCompService> {

    private ContextInfo readContextInfo = new ContextInfo(ContactDetailsConstants.APP_NAME_CONTACT_DETAILS);
    private ApplicationConfig applicationConfig;
    private String appPropertyFileName;
    private static final String SECKEY = "Bar12345Bar12345";
    private static final String INITVECTOR = "RandomInitVector";

    public NotificationCompProxyService(ApplicationConfig applicationConfig, String appPropertyFileName) {
        this.applicationConfig = applicationConfig;
        this.appPropertyFileName = appPropertyFileName;
    }
    
    
    @Override
    protected NotificationCompService initService() throws Exception {

        String nofitificationCompServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
        		NotificationCompServiceNamespace.NOTIFICATION_NOTIFICATION_COMP_SERVICE_KEY,
                ApplicationConfig.NOTIFICATION_API_VERSION,
                null,
                this.appPropertyFileName,
                applicationConfig.getEnvironment());

        log.info("LookupKey: " + nofitificationCompServiceLookupKey + "\n");

        try {
 
            String pws = applicationConfig.getNotificationCompPassword();
            String decryptedPWS = EncryptorUtility.decrypt(SECKEY, INITVECTOR, pws);
            
            return (NotificationCompService) GenericServiceClientFactory.getService(
            		nofitificationCompServiceLookupKey,
                    applicationConfig.getNotificationCompUsername(),
                    decryptedPWS, NotificationCompService.class);

        } catch (Exception ex) {
            log.error("Could not initialize NotificationComp service: " + ex);
            throw new VaadinUIException(ContactDetailMessages.getString("error.notification.fails"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);

        }
    }

    public String sendNotification(NotificationInfo notificationInfo) {
        try {
            ObjectWriter writer = new ObjectMapper().writerWithDefaultPrettyPrinter();

            try {
                log.info("Calling NotificationCompService on method sendNotification with notificationInfo : " +
                        writer.writeValueAsString(notificationInfo));
            } catch (IOException e) {
                log.warn("Could not map request NotificationInfo to string exception: " + e);
            }

            return getService().sendNotification(notificationInfo, readContextInfo);

        } catch (DoesNotExistException doesNotExistException) {
            log.error("Could not find user to send notification to: " + doesNotExistException.getMessage(), doesNotExistException);
            throw new VaadinUIException(ContactDetailMessages.getString("notification.send.notification.not.found"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);
            
           

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("UnforeseenException occurred when sending notification to: " + e.getMessage(), e);
            throw new VaadinUIException(ContactDetailMessages.getString("notification.send.notification.not.found"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);
            
            
        }
    }
}
