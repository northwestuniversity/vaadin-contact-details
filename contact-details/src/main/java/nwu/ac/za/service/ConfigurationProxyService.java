package nwu.ac.za.service;

import ac.za.nwu.utility.EncryptorUtility;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.*;
import nwu.ac.za.config.ApplicationConfig;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.ac.za.util.ContactDetailMessages;
import nwu.ac.za.util.ContactDetailsConstants;
import nwu.enabling.capabilities.supporting.service.configuration.service.ConfigurationService;
import nwu.enabling.capabilities.supporting.service.configuration.service.factory.ConfigurationServiceClientFactory;

import java.util.HashMap;

/**
 * Created by ruhancoetzee on Sep-2021.
 */
public class ConfigurationProxyService extends AbstractServiceProxy<ConfigurationService> {

    private ContextInfo readContextInfo = new ContextInfo(ContactDetailsConstants.APP_NAME_CONTACT_DETAILS);
    private ApplicationConfig applicationConfig;
    private String appPropertyFileName;
    private ConfigurationService configurationService;
    private static final String SECKEY = "Bar12345Bar12345";
    private static final String INITVECTOR = "RandomInitVector";

    public ConfigurationProxyService(ApplicationConfig applicationConfig, String appPropertyFileName) {
        this.applicationConfig = applicationConfig;
        this.appPropertyFileName = appPropertyFileName;
    }

    @Override
    protected ConfigurationService initService() throws Exception {
        try {
            String configurationServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
                    ConfigurationServiceClientFactory.CONFIGURATIONSERVICE,
                    ApplicationConfig.CONFIGVERSION,
                    applicationConfig.getWebserviceDatabase(),
                    this.appPropertyFileName,
                    applicationConfig.getEnvironment());

            String pwsIAPA = applicationConfig.getConfigPassword();
            String decryptedPWS = EncryptorUtility.decrypt(SECKEY, INITVECTOR, pwsIAPA);

            configurationService = ConfigurationServiceClientFactory.getConfigurationService(
                    configurationServiceLookupKey,
                    applicationConfig.getConfigUserName(),
                    decryptedPWS);

        } catch (Exception ex) {
            log.error("Could not connect to Configuration Service: " + ex);
            throw new VaadinUIException(ContactDetailMessages.getString("error.student.fails"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);
        }
        return configurationService;
    }

    public HashMap<String, String> getAllSMSConfig(String authUser) {
        String appRuntimeEnv = applicationConfig.getEnvironment();
        String directoryKey = appRuntimeEnv + "/" + "CONTACT-DETAILS/SMS.CONFIG";
        return setupSMSConfig(directoryKey, authUser);
    }

    private HashMap<String, String> setupSMSConfig(String directoryKey, String authUser) {
        try {
            HashMap<String, String> result = getService().getConfigurationByDirectory(directoryKey, readContextInfo);

            HashMap<String, String> smsHasMap = new HashMap<String, String>();

            for (String config : result.keySet()) {
                String value = result.get(config);
                int lastIndex = config.lastIndexOf("/");
                String actualFaculty = config.substring(lastIndex + 1);
                smsHasMap.put(actualFaculty, value);
            }

            return smsHasMap;
        } catch (DoesNotExistException | InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error(e.getMessage(), e);
            throw new VaadinUIException(ContactDetailMessages.getString("error.student.fails"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);
        }
    }
}
