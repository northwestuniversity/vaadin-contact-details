package nwu.ac.za.service;

import ac.za.nwu.utility.EncryptorUtility;
import assemble.edu.exceptions.*;
import nwu.ac.za.config.ApplicationConfig;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.ac.za.util.ContactDetailMessages;
import nwu.enabling.capabilities.supporting.service.notification.sms.service.SMSService;
import nwu.enabling.capabilities.supporting.service.notification.sms.service.factory.SMSServiceClientFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by ruhancoetzee on Sep-2021.
 */
public class SMSProxyService extends AbstractServiceProxy<SMSService> {

    private ApplicationConfig applicationConfig;
    private String appPropertyFileName;
    private SMSService smsService;

    private static final String SECKEY = "Bar12345Bar12345";
    private static final String INITVECTOR = "RandomInitVector";

    public SMSProxyService(ApplicationConfig applicationConfig, String appPropertyFileName) {
        this.applicationConfig = applicationConfig;
        this.appPropertyFileName = appPropertyFileName;
    }

    @Override
    protected SMSService initService() throws Exception {
        try {
            String smsServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
                    SMSServiceClientFactory.SMSSERVICE,
                    ApplicationConfig.CONFIGVERSION,
                    applicationConfig.getWebserviceDatabase(),
                    this.appPropertyFileName,
                    applicationConfig.getEnvironment());

            String pwsIAPA = applicationConfig.getConfigPassword();
            String decryptedPWS = EncryptorUtility.decrypt(SECKEY, INITVECTOR, pwsIAPA);

            smsService = SMSServiceClientFactory.getSMSService(
                    smsServiceLookupKey,
                    applicationConfig.getConfigUserName(),
                    decryptedPWS);

        } catch (Exception ex) {
            log.error("Could not connect to SMSService: " + ex);
            throw new VaadinUIException(ContactDetailMessages.getString("error.student.fails"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);
        }

        return smsService;
    }

    public String getSMSNotification(String providerUsername, String prodiverPassword, String dialCode, String mobileNumber, String message) {
        try {
            String smsNotification = getService().sendSingleSMSNotification("nwu.service.sms.smsconnect", providerUsername, prodiverPassword, dialCode, mobileNumber, message, null, applicationConfig.getAppName());
            return smsNotification;
        } catch (DoesNotExistException | InvalidParameterException | OperationFailedException | MissingParameterException | PermissionDeniedException e) {
            log.error(e.getMessage(), e);
            throw new VaadinUIException(ContactDetailMessages.getString("error.student.fails"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);
        }
    }
}
