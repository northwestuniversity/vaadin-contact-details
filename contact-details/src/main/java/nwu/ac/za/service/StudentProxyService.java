package nwu.ac.za.service;

import ac.za.nwu.student.dto.StudentSiteInfo;
import ac.za.nwu.student.service.StudentService;
import ac.za.nwu.student.service.factory.StudentServiceClientFactory;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.*;
import nwu.ac.za.config.ApplicationConfig;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.ac.za.util.ContactDetailMessages;
import nwu.ac.za.util.ContactDetailsConstants;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Henriko on 2019-10-03.
 */
public class StudentProxyService extends AbstractServiceProxy<StudentService> {
    private StudentService studentService;
    private ContextInfo readContextInfo = new ContextInfo(ContactDetailsConstants.APP_NAME_CONTACT_DETAILS);

    private ApplicationConfig applicationConfig;
    private String appPropertyFileName;

    public StudentProxyService(ApplicationConfig applicationConfig, String appPropertyFileName) {
        this.applicationConfig = applicationConfig;
        this.appPropertyFileName = appPropertyFileName;
    }

    @Override
    protected StudentService initService() throws Exception {
        try {
            String studentServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
                    StudentServiceClientFactory.STUDENTSERVICE,
                    ApplicationConfig.PROPERTY_NAME_STUDENT_API_VERSION,
                    applicationConfig.getWebserviceDatabase(),
                    this.appPropertyFileName,
                    applicationConfig.getEnvironment());

            studentService = StudentServiceClientFactory.getStudentService(
                    studentServiceLookupKey,
                    applicationConfig.getWsSAPIReadUser(),
                    applicationConfig.getWsSAPIReadUserPassword());
        } catch (Exception e) {
            log.error("Could not initiate Student service: " + e.getMessage());
            throw new VaadinUIException(e, VaadinUIException.SeverityType.ERROR, null, VaadinUIException.Type.SERVERSTARTUPFAILURE, false);
        }
        return studentService;
    }

    public boolean isReturningStudent(String universityNumber, ContextInfo contextInfo) {
        try {
        	if (contextInfo == null) {
				contextInfo = this.readContextInfo;
			} else {
				contextInfo.setSubscriberClientName(ContactDetailsConstants.APP_NAME_CONTACT_DETAILS);
			}
            return getService().isReturningStudent(universityNumber, contextInfo);

        } catch (DoesNotExistException e) {
            log.error("Could not see if the student has been registered for current year: " + e.getMessage());
            return false;

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Unexpected exception occurred while seeing if student are registered : Reference Number 101001: " + e.getMessage());
            throw new VaadinUIException(
                    ContactDetailMessages.getString("error.student.exception"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);

        } catch (Exception e) {
            log.error("Unexpected exception occurred while seeing if student are registered : Reference Number 101002: " + e.getMessage());
            throw new VaadinUIException(
                    ContactDetailMessages.getString("error.student.fails"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);
        }
    }

    public boolean isPostGradStudent(String universityNumber, ContextInfo contextInfo) {
        try {
        	if (contextInfo == null) {
				contextInfo = this.readContextInfo;
			} else {
				contextInfo.setSubscriberClientName(ContactDetailsConstants.APP_NAME_CONTACT_DETAILS);
			}
            return getService().isPostGradStudent(universityNumber, contextInfo);

        } catch (DoesNotExistException e) {
            log.error("Could not see if student are post graduate: " + universityNumber + ". Error: " + e.getMessage());
            throw new VaadinUIException(
                    ContactDetailMessages.getString("error.post.graduate.not.found"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Unforeseen error occurred while checking post graduate status: " + e.getMessage());
            throw new VaadinUIException(
                    ContactDetailMessages.getString("error.post.graduate.failed").concat(": ").concat(e.getMessage()),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);

        } catch (Exception e) {
            log.error("Unforeseen error occurred while checking post graduate status. " + universityNumber + ". Error: " + e.getMessage(), e);
            throw new VaadinUIException(
                    ContactDetailMessages.getString("contact.isPostGradStudent").concat(" ").concat(e.getMessage()),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);
        }
    }

    public StudentSiteInfo getStudentSite(String universityNumber, String date, ContextInfo contextInfo) {
        try {
        	if (contextInfo == null) {
				contextInfo = this.readContextInfo;
			} else {
				contextInfo.setSubscriberClientName(ContactDetailsConstants.APP_NAME_CONTACT_DETAILS);
			}
            return getService().getStudentSite(universityNumber, date, contextInfo);

        } catch (DoesNotExistException e) {
            log.error("Could not find student site: " + e.getMessage());
            throw new VaadinUIException(
                    ContactDetailMessages.getString("error.student.site.not.found"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Unforeseen exception occurred while getting student site: " + e.getMessage());
            throw new VaadinUIException(
                    ContactDetailMessages.getString("error.student.site.fails"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);

        } catch (Exception ex) {
            log.error("Unforeseen exception occurred while getting student site: Reference Number 101003: " + ex.getMessage());
            throw new VaadinUIException(
                    ContactDetailMessages.getString("error.student.site.exception"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);
        }
    }

    public boolean isStudentApprovedAcademicApplicant(String universityNumber, Integer applicationYear, ContextInfo contextInfo) {
        try {
        	if (contextInfo == null) {
				contextInfo = this.readContextInfo;
			} else {
				contextInfo.setSubscriberClientName(ContactDetailsConstants.APP_NAME_CONTACT_DETAILS);
			}
            return getService().isStudentAcademicApplicant(universityNumber, applicationYear, contextInfo);

        } catch (DoesNotExistException e) {
            log.error("Could not find academic applicant: " + e.getMessage());
            throw new VaadinUIException(
                    ContactDetailMessages.getString("error.student.academic.not.found"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Unforeseen exception occurred while getting academic applicant: Reference Number 101004: " + e.getMessage());
            throw new VaadinUIException(
                    ContactDetailMessages.getString("error.student.academic.fails"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);

        } catch (Exception e) {
            log.error("Unforeseen exception occurred while getting academic applicant: Reference Number 101005: " + e.getMessage());
            throw new VaadinUIException(
                    ContactDetailMessages.getString("error.student.academic.exception"),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);
        }
    }

    public Boolean returningStudentOrApprovedApplicant(String lookupUser) {
        // TMC-168 Why not check if it is a employee? 
    	// if(hasStudentRole && !hasEmployeeRole) {

            //Just getting the current year
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            Integer currentYear = cal.get(Calendar.YEAR);

            ContextInfo contextInfo = new ContextInfo();
            contextInfo.setCurrentDate(new Date());

            boolean returningStudent = isReturningStudent(lookupUser, contextInfo);
            boolean studentApprovedApplicantApplicant = false;
            
            // If true then we dont have to do the second database call
            if (returningStudent) {
				return returningStudent;
			} else {
	            studentApprovedApplicantApplicant = isStudentApprovedAcademicApplicant(lookupUser, currentYear , null);				
			}
            
            return returningStudent || studentApprovedApplicantApplicant;
        // }

        // TMC-168 return true;
    }
}

