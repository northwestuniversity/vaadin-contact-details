package nwu.ac.za.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Henriko on 2019-09-11.
 */
abstract public class AbstractServiceProxy<T> {
    public static final Logger log = LoggerFactory.getLogger(AbstractServiceProxy.class);

    private T service;

    public T getService() {
        if (service == null) {
            service = createService();
        }

        return service;
    }

    private T createService() {
        try {
            return initService();
        } catch (Exception e) {
            log.error("Error initializing service", e);
            throw new RuntimeException(e);
        }
    }

    protected abstract T initService() throws Exception;


    public String getServiceRegistryLookupKey(String serviceName, String version, String database, String appRuntimeEnv) throws IOException {

        String serviceLookupKey;

        if (database != null) {
            serviceLookupKey = "/" + appRuntimeEnv + "/" + serviceName + "/" + version + "/" + database;
        } else {
            serviceLookupKey = "/" + appRuntimeEnv + "/" + serviceName + "/" + version;
        }

        return serviceLookupKey.toUpperCase() ;
    }

    public static String getServiceRegistryLookupKeyConfigService(String appRuntimeEnv,
                                                                  String serviceName,
                                                                  String appMajorVersion) {
        String serviceLookupKey;
        serviceLookupKey = "/" + appRuntimeEnv + "/" + serviceName + "/" + appMajorVersion;

        log.info("Lookup key" + serviceLookupKey.toUpperCase());
        return serviceLookupKey.toUpperCase();
    }
}
