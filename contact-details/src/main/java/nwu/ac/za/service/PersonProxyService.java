package nwu.ac.za.service;

import ac.za.nwu.core.person.dto.PersonAffiliationInfo;
import ac.za.nwu.core.person.dto.PersonBiographicInfo;
import ac.za.nwu.core.person.dto.PersonPreferenceInfo;
import ac.za.nwu.core.person.service.PersonService;
import ac.za.nwu.core.person.service.factory.PersonServiceClientFactory;
import ac.za.nwu.utility.EncryptorUtility;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.*;
import nwu.ac.za.config.ApplicationConfig;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.ac.za.util.ContactDetailsConstants;

import java.util.List;

public class PersonProxyService extends AbstractServiceProxy<PersonService> {
    private ApplicationConfig applicationConfig;
    private ContextInfo readContextInfo = new ContextInfo(ContactDetailsConstants.APP_NAME_CONTACT_DETAILS);
    private static final String PERSON_PREFERENCE_CORR_LANGUAGE = "vss.code.TAALDOEL.C";
    // define security keys and initVector
    private static final String SECKEY = "Bar12345Bar12345";
    private static final String INITVECTOR = "RandomInitVector";

    public PersonProxyService(ApplicationConfig applicationConfig) {
        this.applicationConfig = applicationConfig;
    }

    @Override
    protected PersonService initService() throws Exception {
        try {
            String personServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
                    PersonServiceClientFactory.PERSONSERVICE,
                    ApplicationConfig.IAPIVERSION,
                    applicationConfig.getWebserviceDatabase(),
                    applicationConfig.getAppPropertyFileName(),
                    applicationConfig.getEnvironment());
            String pwsIAPA = applicationConfig.getWsIAPIReadPassword();
            String decryptedPWS = EncryptorUtility.decrypt(SECKEY, INITVECTOR, pwsIAPA);
            return PersonServiceClientFactory.getPersonService(personServiceLookupKey, applicationConfig.getIAPIReadUser(), decryptedPWS);

        } catch (Exception e) {
            log.error("Could not initiate personservice: " + e.getMessage(), e);
            throw new VaadinUIException("Could not initiate personservice: " + e.getMessage(), VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.SERVERSTARTUPFAILURE);
        }
    }

    public List<PersonAffiliationInfo> getPersonAffiliation(String lookupUser, ContextInfo contextInfo) {
        try {
        	if (contextInfo == null) {
				contextInfo = this.readContextInfo;
			}
            return getService().getPersonAffiliation(lookupUser,contextInfo);

        } catch (DoesNotExistException e) {
            log.warn("Affiliation for player not found and ignoring it.");
            return null;
        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Could not get Person affiliation: " + e.getMessage(), e);
            throw new VaadinUIException("Could not get Person affiliation: " + e.getMessage(), VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.SERVERSIDE);

        } catch (Exception e) {
            log.error("Unexpected exception while getting Person affiliation: " + e.getMessage(), e);
            throw new VaadinUIException("Unexpected exception while getting Person affiliation: " + e.getMessage(), VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.SERVERSIDE);
        }
    }

    public PersonBiographicInfo getPersonBiographicByLang(String lookupUser, String systemLanguageTypeKey, ContextInfo contextInfo) {
        try {
        	if (contextInfo == null) {
				contextInfo = this.readContextInfo;
			}
            log.info("Calling PersonService on method getPersonBiographicByLang with LookupUser: " + lookupUser + ". \n" +
                    "SystemLanguageTypeKey: " + systemLanguageTypeKey);

            return getService().getPersonBiographicByLang(lookupUser, systemLanguageTypeKey, contextInfo);

        } catch (DoesNotExistException e) {
            log.error("Could not find user: " + lookupUser + ". Error: " + e.getMessage(), e);
            throw new VaadinUIException("Could not find user: " + lookupUser + ". Error: " + e.getMessage(),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Could not retrieve Personal biographic error: " + e.getMessage(), e);
            throw new VaadinUIException("Could not retrieve Personal biographic error: " + e.getMessage(),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);

        } catch (Exception e) {
            log.error("Unforeseen exception while retrieving Personal biographic by language error: " + e.getMessage(), e);
            throw new VaadinUIException("Unforeseen exception while retrieving Personal biographic by language error: " + e.getMessage(),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);
        }
    }

    public List<PersonPreferenceInfo> getPersonPreference(String univNumber, ContextInfo contextInfo) {
        try {
        	if (contextInfo == null) {
				contextInfo = this.readContextInfo;
			}
            return getService().getPersonPreference(univNumber, contextInfo);

        } catch (DoesNotExistException e) {
            log.error("Could not find person preferences: " + e.getMessage(), e);
            throw new VaadinUIException("Could not find person preferences: " + e.getMessage(),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Could not get person preferences: " + e.getMessage(), e);
            throw new VaadinUIException("Could not get person preferences: " + e.getMessage(),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);

        } catch (Exception e) {
            log.error("Unforeseen exception while getting person preferences: " + e.getMessage(), e);
            throw new VaadinUIException("Unforeseen exception while getting person preferences: " + e.getMessage(),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);
        }
    }
    
    public PersonPreferenceInfo getPersonPreferredCorrLanguage(String universityNumber){
        try {
        	PersonPreferenceInfo studentPreferenceInfo = getService().getPersonPreferenceByType(universityNumber, PERSON_PREFERENCE_CORR_LANGUAGE, readContextInfo);
            return studentPreferenceInfo;
        } catch (DoesNotExistException | InvalidParameterException | MissingParameterException | PermissionDeniedException | OperationFailedException e) {
            log.error("Unforeseen exception while getting person preferences: " + e.getMessage(), e);
            throw new VaadinUIException(e.getMessage(),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);
        }
    }
}
