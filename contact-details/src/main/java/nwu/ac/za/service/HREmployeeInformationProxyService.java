package nwu.ac.za.service;

import ac.za.nwu.utility.EncryptorUtility;
import client.employeeinformation.EmployeeAssignmentDetails;
import client.employeeinformation.EmployeeDetailRequest;
import client.employeeinformation.EmployeeInformation;
import nwu.ac.za.HREmployeeInformationServiceClientFactory;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;

import static nwu.ac.za.config.ApplicationConfig.EMPLOYEE_INFORMATION_VERSION;
import static nwu.ac.za.ui.ContactSPCrudUI.INITVECTOR;
import static nwu.ac.za.ui.ContactSPCrudUI.SECKEY;

/*
 Created by Henriko on 2021/07/29 
*/
public class HREmployeeInformationProxyService extends AbstractServiceProxy<EmployeeInformation> {
    private String runtimeEnvironment;
    private String hrPassword;
    private String hrUser;
    private String appPropertyFileName;

    public HREmployeeInformationProxyService(String runtimeEnvironment, String hrPassword, String hrUser, String appPropertyFileName) {
        this.runtimeEnvironment = runtimeEnvironment;
        this.hrPassword = hrPassword;
        this.hrUser = hrUser;
        this.appPropertyFileName = appPropertyFileName;
    }

    @Override
    protected EmployeeInformation initService() throws Exception {
        try {
            String hremployeelookupkey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
                    HREmployeeInformationServiceClientFactory.EMPLOYEEINFORMATION_SERVICE, EMPLOYEE_INFORMATION_VERSION,
                    "", appPropertyFileName, runtimeEnvironment);

            String pwsHR = hrPassword;
            String encryptedPWS = EncryptorUtility.encrypt(SECKEY, INITVECTOR, pwsHR);

            String decryptedPWS = EncryptorUtility.decrypt(SECKEY, INITVECTOR, encryptedPWS);
            log.info("hremployeelookupkey: " + hremployeelookupkey);

            return HREmployeeInformationServiceClientFactory.getEmployeeInformationService(hremployeelookupkey, hrUser,
                    decryptedPWS);
        } catch (Exception e1) {
            log.error("Could not initialize HREmployeeInformationService: " + e1.getMessage(), e1);
            throw new VaadinUIException(e1, VaadinUIException.SeverityType.ERROR, null, VaadinUIException.Type.SERVERSTARTUPFAILURE, false);
        }

    }

    public EmployeeAssignmentDetails getEmployeeDetail(EmployeeDetailRequest employeeDetailRequest) {
        return getService().getEmployeeDetail(employeeDetailRequest);
    }
}
