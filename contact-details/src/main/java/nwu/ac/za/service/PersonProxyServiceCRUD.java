package nwu.ac.za.service;

import ac.za.nwu.core.person.dto.PersonPreferenceInfo;
import ac.za.nwu.core.person.service.PersonServiceCRUD;
import ac.za.nwu.core.person.service.factory.PersonServiceCRUDClientFactory;
import ac.za.nwu.utility.EncryptorUtility;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.*;
import nwu.ac.za.config.ApplicationConfig;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.ac.za.util.ContactDetailsConstants;

/**
 * Created by Henriko on 2019-09-11.
 */
public class PersonProxyServiceCRUD extends AbstractServiceProxy<PersonServiceCRUD> {

    private ApplicationConfig applicationConfig;
    private ContextInfo readContextInfo = new ContextInfo(ContactDetailsConstants.APP_NAME_CONTACT_DETAILS);
    // define security keys and initVector
    private static final String SECKEY = "Bar12345Bar12345";
    private static final String INITVECTOR = "RandomInitVector";

    public PersonProxyServiceCRUD(ApplicationConfig applicationConfig) {
        this.applicationConfig = applicationConfig;
    }

    @Override
    protected PersonServiceCRUD initService() throws Exception {
        try {
            // We need to use supporting services in order to get the ETCD property for database but we need to
            //          upgrade to higher version of Java to that if I am not mistaken.
            String personServiceCrudLookupkey= getServiceRegistryLookupKey(PersonServiceCRUDClientFactory.PERSONSERVICECRUD, "V".concat(applicationConfig.getAPIMajorVersion()), applicationConfig.getWebserviceDatabase(), applicationConfig.getEnvironment());

            String pwsIAPA = applicationConfig.getAPICRUDPassword();
            String encryptedPWS = EncryptorUtility.encrypt(SECKEY, INITVECTOR, pwsIAPA);

            String decryptedPWS = EncryptorUtility.decrypt(SECKEY, INITVECTOR, encryptedPWS);
            return PersonServiceCRUDClientFactory.getPersonServiceCRUD(personServiceCrudLookupkey, applicationConfig.getAPICRUDUserName(), decryptedPWS);

        } catch (Exception ex) {
            log.error("Could not construct PersonProxyService: " + ex.getMessage(), ex);
            throw new VaadinUIException("Could not construct PersonProxyService: " + ex.getMessage(), VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.SERVERSIDE);
        }
    }

    public PersonPreferenceInfo createPersonPreference(PersonPreferenceInfo personPreferenceInfo, ContextInfo contextInfo) {
        try {
        	if (contextInfo == null) {
				contextInfo = this.readContextInfo;
			} else {
				contextInfo.setSubscriberClientName(ContactDetailsConstants.APP_NAME_CONTACT_DETAILS);
			}
            return getService().createPersonPreference(personPreferenceInfo, contextInfo);

        } catch (DoesNotExistException e) {
            log.error("Could not create person preferences, person not found: " + e.getMessage(), e);
            throw new VaadinUIException("Could not create person preferences, person not found: " + e.getMessage(),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Could not create person preferences: " + e.getMessage(), e);
            throw new VaadinUIException("Could not create person preferences: " + e.getMessage(),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);

        } catch (Exception e) {
            log.error("Unforeseen exception: Could not create person preferences: " + e.getMessage(), e);
            throw new VaadinUIException("Unforeseen exception: Could not create person preferences: " + e.getMessage(),
                    VaadinUIException.SeverityType.ERROR,
                    VaadinUIException.Type.SERVERSIDE);

        }
    }
}
