package nwu.ac.za.service;

import ac.za.nwu.utility.EncryptorUtility;

/**
 * Created by Henriko on 2020-02-26.
 */
public class EncryptionTest {

    private static final String SECKEY = "Bar12345Bar12345";
    private static final String INITVECTOR = "RandomInitVector";

    public static void main(String[] agr) {

        System.out.println(EncryptorUtility.decrypt(SECKEY, INITVECTOR, "8KlMC6R0M2jC95mYzG4nvQ=="));
    }
}
