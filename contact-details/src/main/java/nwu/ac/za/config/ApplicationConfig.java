package nwu.ac.za.config;

import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.ac.za.util.ContactDetailsConstants;
import nwu.ac.za.util.PropertyValueRetriever;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

/**
 * Created by Henriko on 2019-09-11.
 */
public class ApplicationConfig {
	private final Logger log = LoggerFactory.getLogger(ApplicationConfig.class.getName());
	private static ApplicationConfig instance;
	private PropertyValueRetriever propertyValueRetriever = new PropertyValueRetriever();
	private String appPropertyFileName;

	public static final String ENVIRONMENT = "runtimeEnvironment";
	public static final String IAPIVERSION = "identity.api.version";
	public static final String CONFIGVERSION = "configuration-management.api.version";
	public static final String PROPERTY_NAME_STUDENT_API_VERSION = "student.api.version";
	public static final String APPNAME = "application.name";
	public static final String EMPLOYEE_INFORMATION_VERSION = "employee_information_version";
	public static final String CONFIG_OTP_SERVICE_VERSION = "config.otp.service.version";
	public static final String SMS_PROVIDER_USERNAME = "SMS.PROVIDER.USERNAME";
	public static final String SMS_PROVIDER_PASSWORD = "sms.provider.password";

	// define security keys and initVector
	private static final String SECKEY = "Bar12345Bar12345";
	private static final String INITVECTOR = "RandomInitVector";

	private String wsAPIUserName;
	private String wsAPIPassword;
	private String iapiMajorVersion;
	private String environment;
	private String configUserName;
	private String configPassword;
	private String configVersion;
	private String configDatabase;

	private String wsIAPIReadUser = null;
	private String wsIAPIReadPassword = null;

	private Map<String, String> appVersionConfiguration;
	private Map<String, String> appGlobalConfiguration;

	private String wsSAPIReadUser = null;
	private String wsSAPIReadUserPassword = null;
	private String appName;

	public final static String NOTIFICATION_API_VERSION = "notification.api.version";
	private final String NOTIFICATION_COMP_USERNAME = "notification.crud.username";
	private final String NOTIFICATION_COMP_PASSWORD = "notification.crud.password";

	private String notificationAPIVersion;
	private String notificationCompUsername;
	private String notificationCompPassword;

	private ApplicationConfig(String appPropertyFileName) {
		this.appPropertyFileName = appPropertyFileName;
	}

	public static ApplicationConfig getInstance(String appPropertyFileName) {
		if (instance == null) {
			instance = new ApplicationConfig(appPropertyFileName);
		}
		return instance;
	}

	public String getAppName() {
		if (appName == null) {
			try {
				appName = propertyValueRetriever.getPropertyValues(APPNAME, appPropertyFileName);

			} catch (IOException e) {
				log.error("Could not get appName: " + e.getMessage());
				throw new VaadinUIException("Could not get appName: " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
			}
		}

		return appName;
	}

	public String getAPICRUDUserName() {
		if (wsAPIUserName == null) {
			try {
				wsAPIUserName = propertyValueRetriever.getPropertyValues(ContactDetailsConstants.WS_IAPI_CRUD_USER_NAME,
						appPropertyFileName);

			} catch (IOException e) {
				log.error("Could not get api username: " + e.getMessage());
				throw new VaadinUIException("Could not get api username: " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
			}
		}

		return wsAPIUserName;
	}

	public String getConfigUserName() {
		if (configUserName == null) {
			try {
				configUserName = propertyValueRetriever
						.getPropertyValues(ContactDetailsConstants.OTPService.CONFIG_OTP_USERNAME, appPropertyFileName);
			} catch (IOException e) {
				log.error("Could not get api username: " + e.getMessage());
				throw new VaadinUIException("Could not get api username: " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
			}
		}
		return configUserName;
	}

	public String getConfigPassword() {
		if (configPassword == null) {
			try {
				configPassword = propertyValueRetriever
						.getPropertyValues(ContactDetailsConstants.OTPService.CONFIG_OTP_PASSWORD, appPropertyFileName);
			} catch (IOException e) {
				log.error("Could not get api username: " + e.getMessage());
				throw new VaadinUIException("Could not get api username: " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
			}
		}
		return configPassword;
	}

	public String getConfigVersion() {
		if (configVersion == null) {
			try {
				configVersion = propertyValueRetriever
						.getPropertyValues(ContactDetailsConstants.OTPService.CONFIG_OTP_VERSION, appPropertyFileName);
			} catch (IOException e) {
				log.error("Could not get api username: " + e.getMessage());
				throw new VaadinUIException("Could not get api username: " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
			}
		}
		return configVersion;
	}

	public String getConfigDatabase() {
		if (configDatabase == null) {
			try {
				configDatabase = propertyValueRetriever
						.getPropertyValues(ContactDetailsConstants.OTPService.CONFIG_OTP_DATABASE, appPropertyFileName);
			} catch (IOException e) {
				log.error("Could not get api username: " + e.getMessage());
				throw new VaadinUIException("Could not get api username: " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
			}
		}
		return configDatabase;
	}

	public String getAPICRUDPassword() {

		if (wsAPIPassword == null) {
			try {
				wsAPIPassword = propertyValueRetriever.getPropertyValues(
						ContactDetailsConstants.WS_IAPI_CRUD_USER_NAME_PASSWORD, appPropertyFileName);

			} catch (IOException e) {
				log.error("Could not get api password: " + e.getMessage());
				throw new VaadinUIException("Could not get api password: " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
			}
		}

		return wsAPIPassword;
	}

	public String getEnvironment() {

		if (environment == null) {
			try {
				environment = propertyValueRetriever.getPropertyValues(ENVIRONMENT, appPropertyFileName);

			} catch (IOException e) {
				log.error("Could not get environment: " + e.getMessage());
				throw new VaadinUIException("Could not get environment: " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
			}
		}

		return environment;
	}

	public String getAPIMajorVersion() {

		if (iapiMajorVersion == null) {
			try {
				iapiMajorVersion = propertyValueRetriever.getPropertyValues(IAPIVERSION, appPropertyFileName);

				if (iapiMajorVersion != null) {
					iapiMajorVersion = iapiMajorVersion.replaceAll("\\..*", "");
				}
			} catch (IOException e) {
				log.error("Could not get IAPI version: " + e.getMessage());
				throw new VaadinUIException("Could not get IAPI version: " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
			}
		}

		return iapiMajorVersion;
	}

	public String getIAPIReadUser() {
		if (wsIAPIReadUser == null) {
			try {
				wsIAPIReadUser = propertyValueRetriever
						.getPropertyValues(ContactDetailsConstants.WS_IAPI_READ_USER_NAME, appPropertyFileName);

			} catch (IOException e) {
				log.error("Could not get IAPI read username: " + e.getMessage());
				throw new VaadinUIException("Could not get IAPI read username: " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
			}
		}

		return wsIAPIReadUser;
	}

	public String getWsIAPIReadPassword() {
		if (wsIAPIReadPassword == null) {
			try {
				wsIAPIReadPassword = propertyValueRetriever.getPropertyValues(
						ContactDetailsConstants.WS_IAPI_READ_USER_NAME_PASSWORD, appPropertyFileName);

			} catch (IOException e) {
				log.error("Could not get IAPI read password: " + e.getMessage());
				throw new VaadinUIException("Could not get IAPI read password: " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.CLIENTSIDE);
			}
		}

		return wsIAPIReadPassword;
	}

	public String getAppPropertyFileName() {
		return appPropertyFileName;
	}

	public String getWebserviceDatabase() {
		// This property is not required - and is only used to overwrite te
		// default database
		String env = getEnvironment();
		if (env.toUpperCase().equals("PROD")) {
			return null;
		}

		return "v_test";
	}

	public String getWsSAPIReadUser() {
		if (wsSAPIReadUser == null) {
			try {
				wsSAPIReadUser = propertyValueRetriever
						.getPropertyValues(ContactDetailsConstants.WS_SAPI_READ_USER_NAME, getAppPropertyFileName());
			} catch (IOException e) {
				log.error("Could not get SAPI username from configuration file: " + e.getMessage(), e);
				throw new VaadinUIException("Could not get SAPI username: " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.SERVERSIDE);
			}
		}

		return wsSAPIReadUser;
	}

	public String getWsSAPIReadUserPassword() {
		if (wsSAPIReadUserPassword == null) {
			try {
				wsSAPIReadUserPassword = propertyValueRetriever.getPropertyValues(
						ContactDetailsConstants.WS_SAPI_READ_USER_NAME_PASSWORD, getAppPropertyFileName());

			} catch (IOException e) {
				log.error("Could not get SAPI password from configuration file: " + e.getMessage(), e);
				throw new VaadinUIException("Could not get SAPI password: " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.SERVERSIDE);

			}
		}

		return wsSAPIReadUserPassword;
	}

	public String getSMS_PROVIDER_USERNAME() {
		try {
			return propertyValueRetriever.getPropertyValues(SMS_PROVIDER_USERNAME, getAppPropertyFileName());
		} catch (IOException e) {
			log.error("Could not get SAPI password from configuration file: " + e.getMessage(), e);
			throw new VaadinUIException("Could not get SAPI password: " + e.getMessage(),
					VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.SERVERSIDE);
		}
	}

	public String getSMS_PROVIDER_PASSWORD() {
		try {
			return propertyValueRetriever.getPropertyValues(SMS_PROVIDER_PASSWORD, getAppPropertyFileName());
		} catch (IOException e) {
			log.error("Could not get SAPI password from configuration file: " + e.getMessage(), e);
			throw new VaadinUIException("Could not get SAPI password: " + e.getMessage(),
					VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.SERVERSIDE);
		}
	}

	public String getNotificationAPIVersion() {
		if (notificationAPIVersion == null || notificationAPIVersion.equals("")) {
			try {
				notificationAPIVersion = propertyValueRetriever.getPropertyValues(NOTIFICATION_API_VERSION,
						getAppPropertyFileName());
			} catch (IOException e) {
				log.error("Could not get notification version API: " + e.getMessage(), e);
				throw new VaadinUIException("Could not get notification version API " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.SERVERSIDE);

			}
		}
		return notificationAPIVersion;
	}

	public String getNotificationCompUsername() {
		if (notificationCompUsername == null || notificationCompUsername.equals("")) {
			try {
				notificationCompUsername = propertyValueRetriever.getPropertyValues(NOTIFICATION_COMP_USERNAME,
						getAppPropertyFileName());
			} catch (IOException e) {
				log.error("Could not get notification username" + e.getMessage(), e);
				throw new VaadinUIException("Could not get notification username " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.SERVERSIDE);
			}
		}
		return notificationCompUsername;
	}

	public String getNotificationCompPassword() {

		if (notificationCompPassword == null || notificationCompPassword.equals("")) {
			try {
				notificationCompPassword = propertyValueRetriever.getPropertyValues(NOTIFICATION_COMP_PASSWORD,
						getAppPropertyFileName());
			} catch (IOException e) {
				log.error("Could not get notification password " + e.getMessage(), e);
				throw new VaadinUIException("Could not get notification password: " + e.getMessage(),
						VaadinUIException.SeverityType.ERROR, VaadinUIException.Type.SERVERSIDE);
			}
		}
		return notificationCompPassword;

	}
}
