package nwu.ac.za.util;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Created by Henriko on 2019-10-03.
 */
public class ContactDetailMessages {
    private static final String BUNDLE_NAME = "nwu/ac/za/messages/ContactDetailsMessages";

    private ContactDetailMessages() {
    }

    public static String getString(String key) {
        try {
            ResourceBundle myResourceBundle = ResourceBundle.getBundle(BUNDLE_NAME + "_" + Locale.getDefault());
            Locale.getDefault();
            return myResourceBundle.getString(key);
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }

    public static String getString(String key, String fileName) {
        try {
            ResourceBundle myResourceBundle = ResourceBundle.getBundle(BUNDLE_NAME + fileName);
            Locale.getDefault();
            return myResourceBundle.getString(key);
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }
}
