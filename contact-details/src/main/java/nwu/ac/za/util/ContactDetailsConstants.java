package nwu.ac.za.util;

/**
 * Created by P21269777 on 2017-05-18.
 */
public class ContactDetailsConstants {

    //Property file values
    public static final String PROPERTY_FILE_NAME = "config.properties";
    public static final String USER_NAME = "username";
    public static final String PASSWORD = "password";
    public static final String WS_IAPI_READ_USER_NAME = "ws_iapiapp_read_username";
    public static final String WS_IAPI_READ_USER_NAME_PASSWORD = "ws_iapiapp_read_username_password";
    public static final String WS_IAPI_CRUD_USER_NAME = "ws_iapiapp_crud_username";
    public static final String WS_IAPI_CRUD_USER_NAME_PASSWORD = "ws_iapiapp_crud_username_password";

    public static final String ERROR_MESSAGE_NO_ACCESS="You are not an active employee or registered student and are unable to utilize the application";

    public static final String ALLOW_UNIV_NUMBER_INPUT = "allowUnivNumberInput";

    //Contact service Purpose Type Keys
    public static final String CONTACT_SERVICE_PHONENUMBERS_CELLPHONE = "vss.code.TELEFOONDOEL.3.Cellphone";
    public static final String CONTACT_SERVICE_PHONENUMBERS_HOME = "vss.code.TELEFOONDOEL.1.Home Number";
    public static final String CONTACT_SERVICE_PHONENUMBERS_WORK = "vss.code.TELEFOONDOEL.2.Work number";
    public static final String CONTACT_SERVICE_PHONENUMBERS_DATA = "vss.code.TELEFOONDOEL.17.Data Number";
    public static final String CONTACT_SERVICE_PHONENUMBERS_ACCOUNT = "vss.code.TELEFOONDOEL.15.Account Cellphone";
    public static final String CONTACT_SERVICE_VIRTUALADDRESS_EMAIL = "vss.code.ADR-TIPE.EMAIL.E-mail";
    public static final String CONTACT_SERVICE_VIRTUALADDRESS_EMAILACC = "vss.code.ADR-TIPE.EMAILACC.Account E-mail";

    public static final String CONTACT_SERVICE_VIRTUALADDRESS_CATEGORY = "vss.code.ADRESKAT.6569";

    public static final String TYPE_KEY_PREFIX = "vss.code";
    public static final String AFFILIATION_INDEX = "HOEDANIG";
    public static final String WS_SAPI_READ_USER_NAME = "ws_sapiapp_read_username";
    public static final String WS_SAPI_READ_USER_NAME_PASSWORD = "ws_sapiapp_read_username_password";
    public static final String APP_NAME_CONTACT_DETAILS = "CONTACT-DETAILS";
    
    //Business Validation Message Constants
    public static class BusinessValidation {
        public static final String VERSION_MISMATCH_ERROR = "version.mismatch.error";
        public static final String DUPLICATE_RECORD_ERROR = "duplicate.record.error";
    }

    //Missing parameter message constants
    public static class MissingParameter {
        public static final String VALUE_REQUIRED = "value.required";
    }

    //General validation message constants
    public static class GeneralValidation {
        public static final String CHARACTER_LENGTH_INVALID = "character.length.invalid";
        public static final String TYPE_KEY_FORMAT_INVALID = "type.key.format.invalid";
        public static final String EMAIL_VALUE_MISMATCH = "E-mail Address and Confirm E-mail Address should match";
        public static final String EMAILACC_VALUE_MISMATCH = "E-mail Address for Account Purposes and Confirm Account E-mail Address should match";
        public static final String PHONE_NUMBER_CELL_INVALID = "Cellphone Number is required";
        public static final String PHONE_NUMBER_WORK_INVALID = "Telephone Number (Work) is required";
        public static final String PHONE_NUMBER_HOME_INVALID = "Telephone Number (Home) is required";
        public static final String PHONE_NUMBER_DATA_INVALID = "Telephone Number (Data) is required";
        public static final String PHONE_NUMBER_ACCOUNT_INVALID = "Telephone Number (Account) is required";
        public static final String PHONE_NUMBER_CELL_DIAL_INVALID = "Cellphone Number International Dial Code is required";
        public static final String PHONE_NUMBER_WORK_DIAL_INVALID = "Telephone Number (Work) International Dial Code is required";
        public static final String PHONE_NUMBER_DATA_DIAL_INVALID = "Telephone Number (Data) International Dail Code is required";
        public static final String PHONE_NUMBER_HOME_DIAL_INVALID = "Telephone Number (Home) International Dial Code is required";
        public static final String PHONE_NUMBER_ACCOUNT_DIAL_INVALID = "Telephone Number (Account) International Dial Code is required";
    }

    // OTP Service constants
    public static class OTPService {
        public static final String CONFIG_OTP_USERNAME = "config.otp.service.username";
        public static final String CONFIG_OTP_PASSWORD = "config.otp.service.password";
        public static final String CONFIG_OTP_VERSION = "config.otp.service.version";
        public static final String CONFIG_OTP_DATABASE = "config.otp.service.database";
    }

    public static class HRAppointmentTypes {
        public static final String PERMANENT = "Permanent Appointment";
        public static final String FIXED_TERM = "Fixed Term Appointment";
        public static final String TEMPORARY = "Temporary Appointment";
        public static final String TEMP_FIXED_TERM_= "Temp Fixed Term Contract";
    }

    public static class HRPositionTypes {
        public static final String STUDENT_ASSISTANT = "Student Assistant";
        public static final String PENSIONER = "Pensioner";
    }

    public static class PersonAffiliationTypes {
        public static final String EMPLOYEE_CODE_INDEX = "W";
        public static final String STUDENT_CODE_INDEX = "S";
        public static final String ALUMNI_CODE_INDEX = "A";
        public static final String EXTERNAL_CODE_INDEX = "E";

        public static final String ALUMNI_TYPE = "Alumni";
        public static final String LEARNER_TYPE = "Learner";
        public static final String EXTERNAL_TYPE = "External";
        public static final String EXTERNAL_TYPE_PERSON_E = "vss.code.HOEDANIG.E.External person(E)";

    }

}
